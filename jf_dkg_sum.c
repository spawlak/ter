#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <openssl/sha.h>
#include <string.h>

#include "jf_dkg_sum.h"

Point *jf_dkg_sum (mpz_t p, mpz_t q, bool random, bool error, bool ERROR, mpz_t g, gmp_randstate_t state, int n, int k) {
  printf("==============================================================\n");
  printf("                   JF-DKG Protocole Simulation \n"); 
  printf("==============================================================\n\n");
  
  int wrong_index = -1;
  int wrong_index_2 = -1;
  bool alreadyHere;
  

  

  /*=====================================================*/
  /*                                                     */
  /*                     COMPUTATION                     */
  /*                                                     */
  /*=====================================================*/

  

  mpz_t *matrix = malloc( n * k * sizeof(mpz_t) );
  mpz_t *matrix_verif = malloc( n * k * sizeof(mpz_t) );
  mpz_t *shares = malloc( n * (n-1) * sizeof(mpz_t) );
  
  mpz_t zq[n];
  mpz_t yq[n];
  
  for (int i = 0; i < n; i++) {
    generate_matrix_poly(matrix, q, k, i, state);
    mpz_init_set(zq[i], matrix[i * k]);

    generate_verification(matrix_verif, matrix, g, p, k, i);
    mpz_init_set(yq[i], matrix_verif[i * k]);
  }

  for (int i = 0; i < n; i++) {
    regroupeAllShares(shares, matrix, q, k, n, i);
  }


  /* error handling*/
  mpz_t nb_rand;
  mpz_init(nb_rand);
  if (error) {
    if (random) {
      wrong_index = rand() % n;
    } else {
      wrong_index = -1;
      while (wrong_index < 0 || wrong_index > (n - 1)) {
        printf("Quel doit etre la mauvaise part (choisissez entre 1 et %d): \n", n);
        scanf("%d", &wrong_index);
        wrong_index--;
      }
    }

    for (int i = 0; i < n; i++) {
      if (i != wrong_index) {
        mpz_urandomm(nb_rand, state, q);

        if (wrong_index > i) {
          mpz_set(shares[i * (n-1) + (wrong_index - 1)], nb_rand);
        } else {
          mpz_set(shares[i * (n-1) + wrong_index], nb_rand);
        }
      }
    }
  }

  mpz_t y;
  mpz_init_set_ui(y, 1);
  for (int i = 0; i < n; i++) {
    mpz_mul(y, y, yq[i]);
    mpz_mod(y, y, p);
  }

  mpz_t verification[k], buf, tmp;
  mpz_inits(buf, tmp, NULL);
  for (int i = 0; i < k; i++) {
    mpz_init_set_ui(verification[i], 1);

    for (int j = 0; j < n; j++) {
      if (j != wrong_index) {
        mpz_mul(tmp, verification[i], matrix_verif[j * k + i]);
        mpz_mod(verification[i], tmp, p);
      }
    }
  }

  mpz_t x[n];
  for (int i = 0; i < n; i++) {
    mpz_init(x[i]);
    if (i != wrong_index) {
      mpz_set_ui(x[i], 0);

      for (int j = 0; j < (n - 1); j++) {
        if ((wrong_index > i && j != (wrong_index - 1)) ||
            (wrong_index < i && j != wrong_index)) {
              mpz_add(x[i], x[i], shares[i * (n-1) + j]);
              mpz_mod(x[i], x[i], q);
        }
      }

      eval_matrix(tmp, matrix, q, k, i, i+1);
      mpz_add(x[i], x[i], tmp);
      mpz_mod(x[i], x[i], q);
    }
  }

  /* Set secret */
  mpz_t secret;
  mpz_init_set_ui(secret, 0);
  for (int i = 0; i < n; i++) {
    if (i != wrong_index) {
      mpz_add(secret, secret, zq[i]);
      mpz_mod(secret, secret, q);
    }
  }

  int *tableau = NULL;
  Point *shares_2 = malloc(k*sizeof(Point));

  if (random) {
    tableau = genererTableauAleatoireWithout(n, k, wrong_index + 1);
    wrong_index_2 = rand() % n;

  } else {
    tableau = (int *)malloc(k * sizeof(int));
    for (int i = 0; i < k; i++) {
      printf("%sChoix de la part %d: ", KNRM, i + 1);
      scanf("%d", &tableau[i]);

      // Gestion d'erreur
      alreadyHere = false;
      for (int j = 0; j < i; j++) {
        if (tableau[i] == tableau[j]) {
          alreadyHere = true;
          break;
        }
      }
      if (tableau[i] > n || tableau[i] < 1 || alreadyHere) {
        printf("vous devez choisir un nombre autre que les précédents entre "
                "%d et %d\n",
                1, n);
        i--;
      }
    }
  }

  for (int i = 0; i < k; i++) {
    mpz_init_set_ui(shares_2[i].x, tableau[i]);
    mpz_init_set(shares_2[i].y, x[tableau[i] - 1]);
  }

  if (ERROR) {
    while (!nombreDejaPresent(tableau, k, wrong_index_2 + 1)) {
      wrong_index_2 = rand() % n;
    }
    mpz_urandomm(nb_rand, state, q);
    mpz_set(shares_2[wrong_index_2].y, nb_rand);

    mpz_urandomm(nb_rand, state, q);
    mpz_set(x[wrong_index_2], nb_rand);
  }

  // Interpolation du polynôme à partir du seuil k
  mpz_t interpolatedSecret;
  mpz_init(interpolatedSecret);
  lagrangeInterpolation(interpolatedSecret, shares_2, k, q);

  /*=====================================================*/
  /*                                                     */
  /*                        PRINTF                       */
  /*                                                     */
  /*=====================================================*/

  gmp_printf("Le generateur vaut %Zd\n", g);

  if (error) {
    printf("ATTENTION %d à envoyé n'importe quoi\n", (wrong_index + 1));
  }


  
   
  if (error) {
    printf("Les participants se rendant compte que %d faisait n'importe quoi, il est exclus !",
            wrong_index + 1);
  }
  printf("\n\n=============================== Final posessed info "
          "===============================\n\n");
  gmp_printf("Le secret vaut %Zd\n\n===============================================\n", secret);

  if (ERROR) {
    printf("Un autre vilain modifie la valeure n°: %d\n", wrong_index_2 + 1);
  }
  for (int i = 0; i < n; i++) {
    if (i != wrong_index) {
      gmp_printf("Partage : (%d, %Zd)\n", i + 1, x[i]);
    }
  }
  
  //calcul de part additives:
  mpz_t lambda, inverse, secret_2, pow;
  mpz_inits(lambda,inverse, secret_2, pow, NULL);
  mpz_set_ui(secret_2,0);

  for (int i = 0; i < k; i++){
    mpz_set_ui(lambda, 1);
    for (int j = 0; j < k; j++){
        if (j != i) {
            mpz_neg(inverse, shares_2[j].x);
            mpz_mul(lambda,lambda, inverse);
            mpz_sub(inverse, shares_2[i].x,shares_2[j].x);
            mpz_sub_ui(pow,q,2);
            mpz_powm(inverse, inverse,pow, q);
            mpz_mul(lambda, inverse, lambda);
            mpz_mod(lambda,lambda,q);

        }
    }
    mpz_mod(lambda,lambda,q);
    mpz_mul(shares_2[i].y,shares_2[i].y, lambda);
    mpz_add(secret_2,secret_2,shares_2[i].y);
    mpz_mod(secret_2,secret_2,q);
  }


  gmp_printf("Le secret vaut %Zd\n\n===============================================\n\n", secret_2);
  return shares_2;
}


void sha256_hash(const char *input, size_t input_len, char outputBuffer[65]) {
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, input, input_len);
    SHA256_Final(hash, &sha256);
    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        sprintf(outputBuffer + (i * 2), "%02x", hash[i]);
    }
    outputBuffer[64] = 0;
}

void sha256_hex_to_mpz(const char *hex_hash, mpz_t result) {
    mpz_init(result);

    int i, j = 0;
    char byte[3];
    byte[2] = '\0';
    unsigned char bytes[32];

    for (i = 0; i < 64; i += 2) {
        byte[0] = hex_hash[i];
        byte[1] = hex_hash[i + 1];
        bytes[j++] = (unsigned char)strtol(byte, NULL, 16);
    }

    mpz_import(result, 32, 1, sizeof(bytes[0]), 0, 0, bytes);
}


Point signature(mpz_t p, mpz_t q){
  int n, t;
  /*=====================================================*/
  /*                                                     */
  /*                     USER'S INPUT                    */
  /*                                                     */
  /*=====================================================*/

  printf("Entrez le nombre total de personnes : ");
  scanf("%d", &n);

  printf("Entrez le seuil de partage : ");
  scanf("%d", &t);

  if (t > n) {
    printf("Le seuil de partage ne peut pas être supérieur au nombre total "
            "de personnes.\n");
    
  }
  gmp_randstate_t state;
  gmp_randinit_mt(state);
  gmp_randseed_ui(state, (unsigned long int)time(NULL));
  /* Generate element of order q */
  mpz_t g, x, k;
  Point signature;
  mpz_inits(g,k,x,signature.x, signature.y, NULL);
  generate_elem_order_q(g, p, q, state);
  Point *z = jf_dkg_sum(p,q,true,false,false, g, state, n , t);
  Point *u = jf_dkg_sum(p,q,true,false,false, g, state,n ,t);
  for (int i = 0; i < t; i++){
    mpz_add(k,k,u[i].y);
    mpz_add(x,x,z[i].y);
  }
  // CALCUL DE C
  mpz_t r, y;
  mpz_inits(r, y, NULL);
  mpz_powm(r,g, k,p);
  mpz_powm(y,g, x,p);
  const char *binary_data = "010101010101010101010101010101010101010101010101";
  // Concaténation de la chaîne binaire et de l'entier GMP
  char concatenated_data[10240]; // Taille suffisamment grande

  // Calcul du hachage SHA-256
  char hash[65];
  sha256_hash(concatenated_data, strlen(concatenated_data), hash);

  sha256_hex_to_mpz(hash, signature.x);

  
  mpz_mod(signature.x,signature.x,q);
  gmp_printf("c result mod q: %Zd\n", signature.x);
  // Libération de la mémoire allouée

  mpz_t tmp;
  mpz_inits(tmp,NULL);
  for (int i = 0; i < t; i++){
    mpz_set(tmp, z[i].y);
    mpz_mul(tmp, signature.x,z[i].y);
    mpz_add(signature.y, tmp, signature.y);
    mpz_add(signature.y, signature.y, u[i].y);
  }
  mpz_mod(signature.y,signature.y,q);

  mpz_t rr, temp, rrr;
  mpz_inits(rr, temp, rrr,NULL);
  mpz_neg(temp,signature.x);
  mpz_powm(temp,y,temp, p);
  mpz_powm(rr, g, signature.y,p);
  mpz_mul(rr,temp,rr);
  char concatenated_data_2[10240]; // Taille suffisamment grande
  gmp_sprintf(concatenated_data_2, "%s%Zd", binary_data, rr);

  // Calcul du hachage SHA-256
  char hash_2[65];
  sha256_hash(concatenated_data_2, strlen(concatenated_data_2), hash_2);


  sha256_hex_to_mpz(hash, rrr);
  mpz_mod(rrr,rrr,q);
  gmp_printf("Verification result mod q: %Zd\n", rrr);






  return signature;
}
