
#include "gmp.h"
#include <getopt.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "shamir.h"
#include "feldman.h"
#include "pedersen.h"
#include "jf_dkg.h"
#include "jf_dkg_sum.h"

#include "ec_feldman.h"
#include "ec_pedersen.h"

#define KRED "\x1B[31m"
#define KNRM "\x1B[0m"
#define QSIZE 256
#define PSIZE 2048

int main(int argc, char *argv[]) {
  // NE PAS TOUCHER CA MARCHE
  gmp_randstate_t state;
  mpz_t n, p, q;

  mpz_inits(p, q, n, NULL);
  gmp_randinit_mt(state);
  // NE PAS TOUCHER CA MARCHE
  gmp_randseed_ui(state, (unsigned long int)time(NULL));
  mpz_urandomb(q, state, QSIZE);
  mpz_nextprime(q, q);

  do {
    mpz_set_ui(p, 1);
    mpz_urandomb(n, state, (PSIZE - QSIZE));
    mpz_mul(p, n, q);
    mpz_add_ui(p, p, 1);
  } while (mpz_probab_prime_p(p, 20) == 0);

  srand(time(NULL));

  bool call_shamir = false;
  bool call_feldman = false;
  bool call_pedersen = false;
  bool random = false;
  bool call_JF_DKG = false;
  bool call_schnorr = false;
  bool call_ec_feldman = false;
  bool call_ec_pedersen = false;

  bool error = false;
  bool ERROR = false;

  int opt;
  int option_index = 0;
  struct option longopts[] = {{"help", no_argument, 0, 'h'},
                              {"version", no_argument, 0, 'v'},
                              {"shamir", no_argument, 0, 's'},
                              {"feldman", no_argument, 0, 'f'},
                              {"pedersen", no_argument, 0, 'p'},
                              {"random", no_argument, 0, 'r'},
                              {"JF_DKG", no_argument, 0, 'd'},
                              {"schnorr", no_argument, 0, 'S'},
                              {"ec_feldman", no_argument, 0, 'F'},
                              {"ec_pedersen", no_argument, 0, 'P'},
                              {"error", no_argument, 0, 'e'},
                              {"ERROR", no_argument, 0, 'E'},
                              // Ajoutez d'autres options au besoin
                              {0, 0, 0, 0}};

  while ((opt = getopt_long(argc, argv, "hvsfrpderESFP", longopts,
                            &option_index)) != -1) {
    switch (opt) {
    case 'h':
      printf("Usage: TER [options]\n");
      printf("Options:\n");
      printf("  -h, --help           Afficher l'aide\n");
      printf("  -v, --version        Afficher la version\n");
      printf("  -s, --shamir         Simule un protocole de shamir\n");
      printf("  -f, --feldman        Simule un protocole de feldman\n");
      printf("  -p, --pedersen       Simule un protocole de pedersen\n");
      printf("  -d, --JF_DKG         Simule un protocole de DKG pedersen\n");
      printf("  -S, --Schnorr        Simule un protocole de signature de Schnorr\n");
      printf("  -F, --ec_feldman     Simule un protocole de Feldman sur courbe"
                                      " elliptique\n");
      printf("  -P, --ec_pedersen    Simule un protocole de Pedersen sur courbe"
                                      " elliptique\n");
      printf("  -r, --random         Simule le choix des parts et de l'erreur "
             "aléatoirement\n");
      printf("  -e, --error          Simule le protocole avec une erreur\n");
      printf("  -E, --ERROR          Simule le protocole avec une erreur a la "
             "deuxieme etape de JF_DKG\n");
      return 0;
    case 'v':
      printf("Sharing_secret_protocoles - Version 1.0\n");
      return 0;
    case 'r':
      random = true;
      break;
    case 'e':
      error = true;
      break;
    case 'E':
      ERROR = true;
      break;
    case 's':
      call_shamir = true;
      break;
    case 'f':
      call_feldman = true;
      break;
    case 'p':
      call_pedersen = true;
      break;
    case 'd':
      call_JF_DKG = true;
      break;
    case 'S':
      call_schnorr = true;
      break;
    case 'F':
      call_ec_feldman = true;
      break;
    case 'P':
      call_ec_pedersen = true;
      break;

    default:
      abort();
    }
  }

  if(call_ec_feldman) {ec_feldman(random, error); return 0;}
  if(call_ec_pedersen) {ec_pedersen(random, error); return 0;}
  
  gmp_printf("(%Zd,%Zd)\n", q, p);
  if (call_shamir) {shamir (p, random); }
  if (call_feldman) {feldman(p, q, random, error); }
  if (call_pedersen) {pedersen(p, q, random, error); }
  if (call_JF_DKG) {jf_dkg(p, q, random, error, ERROR); }
  if (call_schnorr) {
    Point signature_ = signature(p,q);
    gmp_printf("(%Zd,%Zd)\n", signature_.x,signature_.y);
  }

  return 0;
}
