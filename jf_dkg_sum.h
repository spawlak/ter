#include <gmp.h>
#include "aux_func.h"

Point *jf_dkg_sum (mpz_t p, mpz_t q, bool random, bool error, bool ERROR, mpz_t g, gmp_randstate_t state, int n, int k);
Point signature(mpz_t p, mpz_t q);
