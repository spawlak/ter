#include <openssl/ec.h>
#include <openssl/bn.h>
#include <openssl/err.h>
#include <openssl/ecdh.h>
#include <openssl/obj_mac.h>

#define KRED "\x1B[31m"
#define KNRM "\x1B[0m"

typedef struct {
  BIGNUM* x;
  BIGNUM* y;
} Point;

int ec_nombreDejaPresent(int tableau[], int longueur, int nombre);

int* ec_genererTableauAleatoire(int n, int k);

int* ec_genererTableauAleatoireWithout(int n, int k, int i);

void ec_printPolynomial(BIGNUM* coefficients[], int degree);

void ec_generate_polynomial(BIGNUM* coefficients[], BIGNUM* order, int degree);

void ec_evaluate_polynomial(BIGNUM* result, BIGNUM* coefficients[], BIGNUM* order, BIGNUM* point, int degree );

Point* ec_generate_shares(EC_GROUP* curve, EC_POINT* shares_A[], BIGNUM* coefficients[], BIGNUM* order, int threshold, int num_shares);

BIGNUM* ec_lagrange_interpolation(Point shares_2[], BIGNUM* order, int threshold);