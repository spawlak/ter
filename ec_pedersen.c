#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "ec_aux_func.h"

//gcc -o ec_test ec_pedersen.c -lssl -lcrypto
int ec_pedersen(bool random, bool error){
    printf("==============================================================\n");
    printf("           Elliptic curve Pedersen Protocole Simulation       \n"); 
    printf("==============================================================\n\n");

    int num_shares, threshold;
    int nb_error = -1;
    int *tab_error;
    bool alreadyHere;

    /*=====================================================*/
    /*                                                     */
    /*                     USER'S INPUT                    */
    /*                                                     */
    /*=====================================================*/

    printf("Entrez le nombre total de personnes : ");
    scanf("%d", &num_shares);

    printf("Entrez le seuil de partage : ");
    scanf("%d", &threshold);


    if (error) {
        while (nb_error < 0 || nb_error > num_shares) {
        printf("Entrez un nombre d'erreur entre %d et %d : ", 0, num_shares);
        scanf("%d", &nb_error);
        }
        tab_error = (int *)malloc(nb_error * sizeof(int));
    }
    if (threshold > num_shares) {
        printf("Le seuil de partage ne peut pas être supérieur au nombre total"
                "de personnes.\n");
        return 1;
    }

    /*=====================================================*/
    /*                                                     */
    /*                     COMPUTATION                     */
    /*                                                     */
    /*=====================================================*/
    EC_GROUP* curve = EC_GROUP_new_by_curve_name(NID_secp256k1);
    if(curve==0){
        printf("Curve Error\n\n");
    }

    BIGNUM* order = BN_new();
    EC_GROUP_get_order(curve, order, NULL);

    BN_CTX* main_ctx = BN_CTX_new();
    BN_CTX_start(main_ctx);

    /* h a scalar of G, curve generator */
    EC_POINT* h = EC_POINT_new(curve);
    BIGNUM* nb_rand = BN_new();
    BN_rand_range(nb_rand, order);

    EC_POINT_mul(curve, h, nb_rand, 0, 0, main_ctx);

    /* Initialise and generate the polynome */
    BIGNUM** coefficients_1 = malloc(threshold * sizeof(BIGNUM*));
    BIGNUM** coefficients_2 = malloc(threshold * sizeof(BIGNUM*));
    EC_POINT** shares_A1 = malloc(num_shares * sizeof(EC_POINT*));
    EC_POINT** shares_A2 = malloc(num_shares * sizeof(EC_POINT*));
    for (int i = 0; i < threshold; i++) {
        coefficients_1[i] = BN_new();
        coefficients_2[i] = BN_new();
    }
    for (int i = 0; i < num_shares; i++){
        shares_A1[i] = EC_POINT_new(curve);
        shares_A2[i] = EC_POINT_new(curve);
    }

    /* Generate the polynome */
    ec_generate_polynomial(coefficients_1, order, threshold);
    ec_generate_polynomial(coefficients_2, order, threshold);

    /* Set secret */
    BIGNUM* secret = BN_new();
    BN_copy(secret, coefficients_1[0]);

    /* Generate the Shamir's shares */
    Point* shares_1 = ec_generate_shares(curve, shares_A1, coefficients_1, order, threshold, num_shares);
    Point* shares_2 = ec_generate_shares(curve, shares_A2, coefficients_2, order, threshold, num_shares);
    for (int i = 0; i < num_shares; i++){
        EC_POINT_mul(curve, shares_A2[i], 0, shares_A2[i], nb_rand, main_ctx);
    }

    /* error handling */
    if (error) {
        if (random) {
        tab_error = ec_genererTableauAleatoire(num_shares, nb_error);
        for (int i = 0; i < nb_error; i++) {
            tab_error[i]--;
        }

        } else {
        for (int i = 0; i < nb_error; i++) {
            printf("Quel doit etre mauvaise part (choisissez entre 1 et "
                "%d): \n", num_shares);
            scanf("%d", &tab_error[i]);
            tab_error[i]--;

            if (ec_nombreDejaPresent(tab_error, i, tab_error[i]))
                i--;
            }
        }

        for (int i = 0; i < nb_error; i++) {
            BN_rand_range(shares_1[i].y, order);

            BN_rand_range(shares_2[i].y, order);
        }
    }

    EC_POINT* verification[threshold];
    EC_POINT* left = EC_POINT_new(curve);
    EC_POINT* right = EC_POINT_new(curve);
    for (int i = 0; i < threshold; i++) {
        verification[i] = EC_POINT_new(curve);
        EC_POINT_mul(curve, left, coefficients_1[i], 0, 0, main_ctx);
        EC_POINT_mul(curve, right, 0, h, coefficients_2[i], main_ctx);

        EC_POINT_add(curve, verification[i], left, right, main_ctx);
    }
    /* Choice of shares for sharing */
    Point shares_3[threshold];
    int *tableau = NULL;

    if (random) {
        tableau = ec_genererTableauAleatoire(num_shares, threshold);

    } else {
        tableau = (int *)malloc(threshold * sizeof(int));
        for (int i = 0; i < threshold; i++) {
            if (error) {
                for (int i = 0; i < nb_error; i++) {
                    printf("\n%sLa mauvaise part est la %d\n", KRED, tab_error[i] + 1);
                }
            }
            printf("%sChoix de la part %d: ", KNRM, i + 1);
            scanf("%d", &tableau[i]);

            // Gestion d'erreur
            alreadyHere = false;
            for (int j = 0; j < i; j++) {
                if (tableau[i] == tableau[j]) {
                    alreadyHere = true;
                    break;
                }
            }
            if (tableau[i] > num_shares || tableau[i] < 1 || alreadyHere) {
                printf("Vous devez choisir un nombre autre que les précédents entre"
                    "%d et %d\n", 1, num_shares);
                i--;
            }
        }
    }
    printf("\n");

    for (int i = 0; i < threshold; i++) {
        shares_3[i] = shares_1[tableau[i] - 1];
    }

    /* Interpolation of the polynomial from the threshold k*/
    BIGNUM* interpolatedSecret = BN_new();
    interpolatedSecret = ec_lagrange_interpolation(shares_3, order, threshold);

    /*=====================================================*/
    /*                                                     */
    /*                        PRINTF                       */
    /*                                                     */
    /*=====================================================*/

    printf("\nLe secret vaut: ");
    BN_print_fp(stdout, secret);
    
    char* value_h = EC_POINT_point2hex(curve, h, POINT_CONVERSION_COMPRESSED, NULL);
    printf("\n\nh est une puissance de g et vaut: %s\n", value_h);

    // Affichage du polynôme généré
    ec_printPolynomial(coefficients_1, threshold);
    ec_printPolynomial(coefficients_2, threshold);
    
/*  printf("\n\n=============================\n");
    for (int i = 1; i <= num_shares; i++) {
        if (!ec_nombreDejaPresent(tab_error, nb_error, i - 1)) {
        //printf("%sPartage %d : (%Zd, %Zd, %Zd)\n", KNRM, i, shares_1[i - 1].x,shares_1[i - 1].y, shares_2[i - 1].y);
        } else {
        //printf("%sMauvais partage %d : (%Zd, %Zd, %Zd)\n", KRED, i,shares_1[i - 1].x, shares_1[i - 1].y, shares_2[i - 1].y);
        }
    }
    printf("%s=============================\n\n", KNRM);*/

    printf("Le dealer broadcast aussi a chacun A_k = a_k * G:\n");
    char * hex_point;
    for (int i = 0; i < threshold; i++) {
        hex_point = EC_POINT_point2hex(curve, verification[i], POINT_CONVERSION_COMPRESSED, NULL);
        printf("Point: %s\n", hex_point);
    }

    printf("\n\n                VERIFICATION                \n\n");
    BIGNUM* pow = BN_new();
    BIGNUM* j_index = BN_new();
    EC_POINT* sum = EC_POINT_new(curve);
    EC_POINT* right2 = EC_POINT_new(curve);
    EC_POINT* left2 = EC_POINT_new(curve);
    
    for (int i = 0; i < num_shares; i++) {
        EC_POINT_mul(curve, left, shares_1[i].y, 0, 0, main_ctx);
        EC_POINT_mul(curve, left2, 0, h, shares_2[i].y, main_ctx);

        BN_zero(j_index);
        EC_POINT_set_to_infinity(curve, right);
        EC_POINT_set_to_infinity(curve, right2);

        for (int j = 0; j < threshold; j++) {
            BN_mod_exp(pow, shares_1[i].x, j_index, order, main_ctx);
            EC_POINT_mul(curve, sum, 0, shares_A1[j], pow, main_ctx);

            EC_POINT_add(curve, right, right, sum, main_ctx);

            BN_mod_exp(pow, shares_2[i].x, j_index, order, main_ctx);
            EC_POINT_mul(curve, sum, 0, shares_A2[j], pow, main_ctx);

            EC_POINT_add(curve, right2, right2, sum, main_ctx);


            BN_add_word(j_index, 1);
        }

        if (EC_POINT_cmp(curve, left, right, main_ctx) ||
            EC_POINT_cmp(curve, left2, right2, main_ctx)){
            printf("%s Menteur ! Il faut discalifier le joueur %u.\n\n", KRED, i + 1);
        }
        else{
            printf("%s Le joueur %u n'est pas un menteur.\n\n", KNRM, i + 1);
        }
    }
    /*
    for (int i = 0; i < threshold; i++) {
        if (!(nombreDejaPresent(tab_error, nb_error, tableau[i] - 1))) {
        gmp_printf("%s\nOn utilisera la part %d: (%Zd,%Zd,%Zd)\n", KNRM, tableau[i],
                shares_1[tableau[i] - 1].x, shares_1[tableau[i] - 1].y,
                shares_2[tableau[i] - 1].y);
        } else {
        gmp_printf("%s\nOn utilisera la part %d: (%Zd,%Zd,%Zd)\n", KRED, tableau[i],
                shares_1[tableau[i] - 1].x, shares_1[tableau[i] - 1].y,
                shares_2[tableau[i] - 1].y);
        }
    }
    }*/

    if (BN_cmp(secret, interpolatedSecret)){
        printf("\n%sSecret mal interpolé: ", KRED);
        BN_print_fp(stdout, interpolatedSecret);
    }
    else{
        printf("\n%sSecret correctement interpolé: ", KNRM);
        BN_print_fp(stdout, interpolatedSecret);
    }
    printf("\n");
    
    /* Free zone */
    for (int i = 0; i < threshold; i++){
        BN_free(coefficients_1[i]);
        BN_free(coefficients_2[i]);
    }
    for (int i = 0; i < num_shares; i++){
        EC_POINT_free(shares_A1[i]);
        EC_POINT_free(shares_A2[i]);

    }

    BN_free(secret);
    BN_free(interpolatedSecret);
    BN_free(order);
    BN_free(pow);
    BN_free(j_index);
    BN_free(nb_rand);

    EC_POINT_free(right);
    EC_POINT_free(left);
    EC_POINT_free(sum);

    EC_GROUP_free(curve);

    BN_CTX_end(main_ctx);
    BN_CTX_free(main_ctx); 
    
    return 0;
}
