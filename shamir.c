#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "aux_func.h"

int shamir(mpz_t p, bool random){
    printf("==============================================================\n");
    printf("                   Shamir Protocole Simulation                \n");
    printf("==============================================================\n\n");

    int n, k;
    bool alreadyHere;

    /*=====================================================*/
    /*                                                     */
    /*                     USER'S INPUT                    */
    /*                                                     */
    /*=====================================================*/

    printf("Entrez le nombre total de personnes : ");
    scanf("%d", &n);

    printf("Entrez le seuil de partage : ");
    scanf("%d", &k);

    if (k > n || k < 1) {
      printf("Le seuil de partage ne peut pas être supérieur au nombre total "
             "de personnes.\n");
      return 1;
    }

    /*=====================================================*/
    /*                                                     */
    /*                     COMPUTATION                     */
    /*                                                     */
    /*=====================================================*/

    
    // Génération aléatoire des coefficients du polynôme
    mpz_t coefficients[n];
    mpz_t nb_rand;
    gmp_randstate_t state;
    mpz_init(nb_rand);

    gmp_randinit_mt(state);
    gmp_randseed_ui(state, (unsigned long int)time(NULL));

    for (int i = 0; i < k; i++) {
      mpz_init(coefficients[i]);
      mpz_urandomm(nb_rand, state, p);
      mpz_set(coefficients[i], nb_rand);
    }
    mpz_t secret;
    mpz_init_set(secret, coefficients[0]);

    // Génération des partages de Shamir
    Point *shares = generateShares(coefficients, n, k, p);

    // choix d'un certain nombre de part:
    Point shares_2[k];
    int *tableau = NULL;

    if (random) {
      tableau = genererTableauAleatoire(n, k);
    } else {
      tableau = (int *)malloc(k * sizeof(int));
      for (int i = 0; i < k; i++) {
        printf("choix de la part %d:", i + 1);
        scanf("%d", &tableau[i]);

        // Gestion d'erreur
        alreadyHere = false;
        for (int j = 0; j < i; j++) {
          if (tableau[i] == tableau[j]) {
            alreadyHere = true;
            break;
          }
        }
        if (tableau[i] > n || tableau[i] < 1 || alreadyHere) {
          printf("vous devez choisir un nombre autre que les précédents entre "
                 "%d et %d\n",
                 1, n);
          i--;
        }
      }
    }
    for (int i = 0; i < k; i++) {
      shares_2[i] = shares[tableau[i] - 1];
    }

    // Interpolation du polynôme à partir du seuil k
    mpz_t interpolatedSecret;
    mpz_init(interpolatedSecret);
    lagrangeInterpolation(interpolatedSecret, shares_2, k, p);

    /*=====================================================*/
    /*                                                     */
    /*                        PRINTF                       */
    /*                                                     */
    /*=====================================================*/

    gmp_printf("\nLe secret vaut %Zd", secret);
    printPolynomial(coefficients, k - 1);
    printf("\n=============================\n");
    for (int i = 1; i <= n; i++) {
      gmp_printf("Partage %d : (%Zd, %Zd)\n", i, shares[i - 1].x,
                 shares[i - 1].y);
    }

    printf("=============================\n\n");
    for (int i = 0; i < k; i++) {
      gmp_printf("On utilisera la part %d: (%Zd,%Zd)\n", tableau[i],
                 shares_2[i].x, shares_2[i].y);
    }
    gmp_printf("\nLe secret interpolé est %Zd\n", interpolatedSecret);
    
    return 0;
}