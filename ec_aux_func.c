#include "ec_aux_func.h"


int ec_nombreDejaPresent(int tableau[], int longueur, int nombre) {
  for (int i = 0; i < longueur; i++) {
    if (tableau[i] == nombre) {
      return 1; // Le nombre est déjà présent
    }
  }
  return 0; // Le nombre n'est pas encore présent
}

int* ec_genererTableauAleatoire(int n, int k) {
  if (k > n) {
    printf("Erreur : k ne peut pas être plus grand que n.\n");
    return NULL;
  }

  int *tableau = (int *)malloc(k * sizeof(int));
  if (tableau == NULL) {
    printf("Erreur d'allocation mémoire.\n");
    return NULL;
  }

  int index = 0;

  while (index < k) {
    int nombreAleatoire = rand() % n + 1; // Génère un nombre entre 1 et n

    // Vérifie si le nombre est déjà présent dans le tableau
    if (!ec_nombreDejaPresent(tableau, index, nombreAleatoire)) {
      tableau[index] = nombreAleatoire;
      index++;
    }
  }

  return tableau;
}

int* ec_genererTableauAleatoireWithout(int n, int k, int i) {
  int *tableau = (int *)malloc(k * sizeof(int));
  if (tableau == NULL) {
    printf("Erreur d'allocation mémoire.\n");
    return NULL;
  }

  int index = 0;

  while (index < k) {
    int nombreAleatoire = rand() % n + 1; // Génère un nombre entre 1 et n

    // Vérifie si le nombre est déjà présent dans le tableau
    if (!ec_nombreDejaPresent(tableau, index, nombreAleatoire) &&
        !(nombreAleatoire == i)) {
      tableau[index] = nombreAleatoire;
      index++;
    }
  }

  return tableau;
}

// Fonction pour afficher un polynôme
void ec_printPolynomial(BIGNUM* coefficients[], int degree) {
    printf("\nPolynôme : ");
    for (int i = (degree - 1); i > 0; i--) {
        if (BN_cmp(coefficients[i], 0)) {
            if (i < (degree - 1)) {
                printf(" + ");
            }
            BN_print_fp(stdout, coefficients[i]);
            printf(" X^%d", i);
        }
    }

    if (BN_cmp(coefficients[0], 0)) {
        printf(" + \n");
        BN_print_fp(stdout, coefficients[0]);
    }
}

/* Generation of the polynome and matrix of polynome */
void ec_generate_polynomial(BIGNUM* coefficients[], BIGNUM* order, int degree) {
  for (int i = 0; i < degree; i++) {
    BN_rand_range(coefficients[i], order);
  }
}

// Evaluate the polynomial at point `x` using Horner's method
void ec_evaluate_polynomial(BIGNUM* result, BIGNUM* coefficients[], BIGNUM* order, BIGNUM* point, int degree ) {
    BIGNUM* save = BN_new();
    BIGNUM* coefficient = BN_new();
    BIGNUM* eval = BN_new();

    BN_CTX* eval_ctx = BN_CTX_new();
    BN_CTX_start(eval_ctx);

    BN_zero(save);

    for (int i = (degree - 1); i >= 0; i--) {
        BN_set_word(eval, i);

        BN_mod_exp(eval, point, eval, order, eval_ctx);
        BN_mod_mul(coefficient, eval, coefficients[i], order, eval_ctx);

        BN_mod_add(save, save, coefficient, order, eval_ctx);
    }
    
    BN_copy(result, save);
    BN_CTX_end(eval_ctx);
    BN_CTX_free(eval_ctx);
}

// Generate shares for the secret using Shamir Secret Sharing
Point* ec_generate_shares(EC_GROUP* curve, EC_POINT* shares_A[], BIGNUM* coefficients[], BIGNUM* order, int threshold, int num_shares){
    Point *shares = (Point *)malloc(num_shares * sizeof(Point));
    for(int i=0;i<num_shares;i++){
        shares[i].x = BN_new();
        shares[i].y = BN_new();
    }
    BIGNUM* i_value = BN_new();
    BN_CTX* gen_ctx = BN_CTX_new();
    BN_CTX_start(gen_ctx);

    BN_one(i_value);
    for (int i = 0; i < num_shares; i++) {    
        BN_set_word(shares[i].x, i + 1);
        ec_evaluate_polynomial(shares[i].y, coefficients, order, i_value, threshold);

        BN_add_word(i_value, 1);
    }

    for (int i = 0; i < threshold; i++){
        EC_POINT_mul(curve, shares_A[i], coefficients[i], 0, 0, gen_ctx); 
    }

    BN_CTX_end(gen_ctx);
    BN_CTX_free(gen_ctx);
    return shares;
}

BIGNUM* ec_lagrange_interpolation(Point shares_2[], BIGNUM* order, int threshold){
    BIGNUM* result = BN_new();
    BIGNUM* tmp = BN_new();
    BIGNUM* inverse = BN_new();
    BIGNUM* numerator = BN_new();

    BN_CTX* interpol_ctx = BN_CTX_new();
    BN_CTX_start(interpol_ctx);

    for (int i = 0; i < threshold; i++) {
        BN_copy(numerator, shares_2[i].y);
        BN_one(inverse);

        for (int j = 0; j < threshold; j++) {
            if (i == j)
                continue;

            BN_copy(tmp, shares_2[j].x);
            BN_set_negative(tmp, 1);

            BN_mod_mul(numerator, numerator, tmp, order, interpol_ctx);
            
            BN_copy(tmp, shares_2[j].x);
            BN_sub(tmp, tmp, shares_2[i].x);

            BN_mod_mul(inverse, inverse, tmp, order, interpol_ctx);
        }

        BN_copy(tmp, order);
        BN_sub_word(tmp, 2);

        BN_mod_exp(inverse, inverse, tmp, order, interpol_ctx);

        BN_mod_mul(numerator, numerator, inverse, order, interpol_ctx);

        BN_mod_add(result, result, numerator, order, interpol_ctx);
    }

    return result;
}
