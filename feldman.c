#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "aux_func.h"

int feldman(mpz_t p, mpz_t q, bool random, bool error) {
  printf("==============================================================\n");
  printf("                  Feldman Protocole Simulation \n");
  printf("==============================================================\n\n");

  int n, k;
  int nb_error = -1;
  int *tab_error;
  bool alreadyHere;
  gmp_randstate_t state;
  gmp_randinit_mt(state);
  gmp_randseed_ui(state, (unsigned long int)time(NULL));

  /*=====================================================*/
  /*                                                     */
  /*                     USER'S INPUT                    */
  /*                                                     */
  /*=====================================================*/

  printf("Entrez le nombre total de personnes : ");
  scanf("%d", &n);

  printf("Entrez le seuil de partage : ");
  scanf("%d", &k);

  if (error) {
    while (nb_error < 0 || nb_error > n) {
      printf("Entrez un nombre d'erreur entre %d et %d : ", 0, n);
      scanf("%d", &nb_error);
    }
    tab_error = (int *)malloc(nb_error * sizeof(int));
  }
  if (k > n) {
    printf("Le seuil de partage ne peut pas être supérieur au nombre total"
            "de personnes.\n");
    return 1;
  }

  /*=====================================================*/
  /*                                                     */
  /*                     COMPUTATION                     */
  /*                                                     */
  /*=====================================================*/

  // Génération aléatoire des coefficients du polynôme
  mpz_t coefficients[n];
  mpz_t nb_rand;
  mpz_init(nb_rand);
  for (int i = 0; i < k; i++) {
    mpz_init(coefficients[i]);
    mpz_urandomm(nb_rand, state, q);//p ou q ? //////////////////////
    mpz_set(coefficients[i], nb_rand);
  }

  mpz_t secret;
  mpz_init_set(secret, coefficients[0]);


  // Recherche d'un element d'ordre q
  mpz_t g;
  mpz_init(g);
  generate_elem_order_q(g,p,q,state);
   
  // Génération des partages de Shamir
  Point *shares = generateShares(coefficients, n, k, p);

  // Gereration de l'erreur
  if (error) {
    if (random) {

      tab_error = genererTableauAleatoire(n, nb_error);
      for (int i = 0; i < nb_error; i++) {
        tab_error[i]--;
      }

    } else {
      for (int i = 0; i < nb_error; i++) {
        printf("Quel doit etre mauvaise part (choisissez entre 1 et "
                "%d): \n", n);
        scanf("%d", &tab_error[i]);
        tab_error[i]--;

        if (nombreDejaPresent(tab_error, i, tab_error[i]))
          i--;
      }
    }

    for (int i = 0; i < nb_error; i++) {
      mpz_urandomm(nb_rand, state, q); //p ou q ???
      mpz_set(shares[tab_error[i]].y, nb_rand);
    }
  }

  mpz_t verification[k];
  for (int i = 0; i < k; i++) {
    mpz_init(verification[i]);
    mpz_powm(verification[i], g, coefficients[i], p);
  }

  // choix d'un certain nombre de part aleatoirement:
  Point shares_2[k];
  int *tableau = NULL;

  if (random) {
    tableau = genererTableauAleatoire(n, k);

  } else {
    tableau = (int *)malloc(k * sizeof(int));
    for (int i = 0; i < k; i++) {
      if (error) {
        for (int i = 0; i < nb_error; i++) {
          printf("\n%sla mauvaise part est la %d\n", KRED, tab_error[i] + 1);
        }
      }
      printf("%schoix de la part %d: ", KNRM, i + 1);
      scanf("%d", &tableau[i]);

      // Gestion d'erreur
      alreadyHere = false;
      for (int j = 0; j < i; j++) {
        if (tableau[i] == tableau[j]) {
          alreadyHere = true;
          break;
        }
      }
      if (tableau[i] > n || tableau[i] < 1 || alreadyHere) {
        printf("vous devez choisir un nombre autre que les précédents entre "
                "%d et %d\n", 1, n);
        i--;
      }
    }
  }
  for (int i = 0; i < k; i++) {
    shares_2[i] = shares[tableau[i] - 1];
  }

  // Interpolation du polynôme à partir du seuil k
  mpz_t interpolatedSecret;
  mpz_init(interpolatedSecret);
  lagrangeInterpolation(interpolatedSecret, shares_2, k, p);

  /*=====================================================*/
  /*                                                     */
  /*                        PRINTF                       */
  /*                                                     */
  /*=====================================================*/

  gmp_printf("\nle secret vaut %Zd\n", secret);

  // Affichage du polynôme généré
  printPolynomial(coefficients, k - 1);

  gmp_printf("\n\nle générateur vaut %Zd\n", g);

  printf("\n\n=============================\n");
  for (int i = 0; i < n; i++) {
    if (!nombreDejaPresent(tab_error, nb_error, i - 1)) {
      gmp_printf("%sPartage %d : (%Zd, %Zd)\n", KNRM, i + 1, shares[i].x,
                  shares[i].y);
    } else {
      gmp_printf("%sMauvais partage %d : (%Zd, %Zd)\n", KRED, i + 1,
                  shares[i].x, shares[i].y);
    }
  }
  printf("%s=============================\n\n", KNRM);

  printf("Le dealer broadcast aussi a chacun:\n");

  for (int i = 0; i < k; i++) {
    gmp_printf("%Zd, ", verification[i]);
  }

  printf("\n\n                VERIFICATION                \n\n");
  mpz_t left, sum, pow, temps;
  mpz_inits(left, sum, pow, temps, NULL);

  for (int i = 0; i < n; i++) {
    mpz_set_ui(sum, 1);
    mpz_set_ui(pow, 1);

    for (int j = 0; j < k; j++) {
      mpz_powm(temps, verification[j], pow, p);
      mpz_mul(sum, sum, temps);
      mpz_mod(sum, sum, p);
      
      mpz_mul_ui(pow, pow, i + 1);
      mpz_mod(pow, pow, q);
    }

    mpz_powm(left, g, shares[i].y, p);
    if (mpz_cmp(left, sum)){
      gmp_printf("%sMenteur %Zd = %Zd\n\n", KRED, left, sum);
    }
    else{
      gmp_printf("%s%Zd = %Zd\n\n", KNRM, left, sum);
    }
  }

  printf("\n");
  for (int i = 0; i < k; i++) {
    if (!(nombreDejaPresent(tab_error, nb_error, tableau[i] - 1))) {
      gmp_printf("%son utilisera la part %d: (%Zd,%Zd)\n", KNRM, tableau[i],
                  shares_2[i].x, shares_2[i].y);
    } else {
      gmp_printf("%son utilisera la part %d: (%Zd,%Zd)\n", KRED, tableau[i],
                  shares_2[i].x, shares_2[i].y);
    }
  }

  gmp_printf("\n%sSecret interpolé : %Zd\n", KNRM, interpolatedSecret);
  return 0;
}
