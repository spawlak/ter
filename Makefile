CFLAGS = -std=c11 -Wall -Wextra -g -O2
CPPFLAGS = -I../include -DDEBUG
LDFLAGS = -lm -lgmp -lssl -lcrypto

all: main

main: main.o aux_func.o ec_aux_func.o shamir.o feldman.o pedersen.o jf_dkg.o jf_dkg_sum.o ec_feldman.o ec_pedersen.o 
	$(CC) $(CFLAGS) -fopenmp -o $@ $^ $(LDFLAGS)

main.o: main.c main.h aux_func.h ec_aux_func.h shamir.h feldman.h pedersen.h jf_dkg.h jf_dkg_sum.h ec_feldman.h ec_pedersen.h 
	$(CC) $(CFLAGS) -fopenmp $(CPPFLAGS) -c $<


aux_func.o: aux_func.c aux_func.h
	$(CC) $(CFLAGS) -fopenmp $(CPPFLAGS) -c $< 

ec_aux_func.o: ec_aux_func.c ec_aux_func.h
	$(CC) $(CFLAGS) -fopenmp $(CPPFLAGS) -c $< 


shamir.o: shamir.c  shamir.h aux_func.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

feldman.o: feldman.c feldman.h aux_func.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

pedersen.o: pedersen.c pedersen.h aux_func.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

jf_dkg.o: jf_dkg.c jf_dkg.h aux_func.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

jf_dkg_sum.o: jf_dkg_sum.c jf_dkg_sum.h aux_func.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<


ec_feldman.o: ec_feldman.c ec_feldman.h ec_aux_func.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

ec_pedersen.o: ec_pedersen.c ec_pedersen.h ec_aux_func.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

clean:
	@rm -f *.o
	@rm -f main

.PHONY: all clean 
