#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "aux_func.h"

void generate_poly(mpz_t* coefficients, mpz_t q, int k, gmp_randstate_t state){
  mpz_t nb_rand;
  mpz_init(nb_rand);

  for (int i = 0; i < k; i++) {
    mpz_urandomm(nb_rand, state, q);
    mpz_init_set(coefficients[i], nb_rand);
  }
}

void generate_matrix_poly(mpz_t* matrix, mpz_t q, int k, int index, gmp_randstate_t state){
  mpz_t nb_rand;
  mpz_init(nb_rand);

  for(int i = 0; i < k ; i++){
    mpz_urandomm(nb_rand, state, q);
    mpz_init_set(matrix[index * k + i], nb_rand);
  }
}

void generate_elem_order_q (mpz_t g, mpz_t p, mpz_t q, gmp_randstate_t state){
  mpz_t buff;
  mpz_init(buff);

  while (mpz_cmp_ui(g, 1) && !mpz_cmp_ui(g, 0)) {
    mpz_urandomm(g, state, p);
    mpz_sub_ui(buff, p, 1);
    mpz_div(buff, buff, q);
    mpz_powm(g, g, buff, p);
  }
}

// Fonction pour afficher un polynôme
void printPolynomial(mpz_t* coefficients, int degree) {
    printf("\n\nPolynôme : ");
    for (int i = degree; i > 0; i--) {
        if (mpz_cmp_ui(coefficients[i], 0)) {
            if (i < degree) {
                printf(" + ");
            }
            gmp_printf("%Zd X^%d", coefficients[i], i);
        }
    }

    if (mpz_cmp_ui(coefficients[0], 0)) {
        gmp_printf(" + %Zd\n", coefficients[0]);
    }
}

// Fonction pour afficher un polynôme
void print_matrix (mpz_t* matrix, int k, int index) {
    printf("\n\nPolynôme : ");
    for (int i = (k - 1); i > 0; i--) {
        if (mpz_cmp_ui(matrix[index * k + i], 0)) {
            if (i < (k - 1)) {
                printf(" + ");
            }
            gmp_printf("%Zd X^%d", matrix[index * k + i], i);
        }
    }

    if (mpz_cmp_ui(matrix[index * k], 0)) {
        gmp_printf(" + %Zd\n", matrix[index * k]);
    }
}

void evaluatePolynome(mpz_t result, mpz_t *coefficient, mpz_t prime, int point,
                      int k) {
  mpz_t evaluation;
  mpz_inits(result, evaluation, NULL);  

  for (int i = 0; i < k; i++) {
    mpz_set_ui(evaluation, point);
    mpz_powm_ui(evaluation, evaluation, i, prime);
    mpz_mul(evaluation, coefficient[i], evaluation);
    mpz_add(result, result, evaluation);
    mpz_mod(result, result, prime);
  }
}

// Fonction pour calculer les partages de Shamir
Point *generateShares(mpz_t *coefficients, int n, int k, mpz_t prime) {
  Point *shares = (Point *)malloc(n * sizeof(Point));

  for (int i = 1; i <= n; i++) {
    mpz_init_set_ui(shares[i - 1].x, i);
    mpz_init_set_ui(shares[i - 1].y, 0);
    evaluatePolynome(shares[i - 1].y, coefficients, prime, i, k);
  }

  return shares;
}

void lagrangeInterpolation(mpz_t result, Point *shares, int k, mpz_t prime) {
  mpz_t term, blabla, inverse_i;
  mpz_init(blabla);
  mpz_init(term);
  mpz_init(inverse_i);
  for (int i = 0; i < k; i++) {
    mpz_set(term, shares[i].y);
    mpz_set_ui(inverse_i, 1); // Lorsque x est 0, la puissance est toujours 0

    for (int j = 0; j < k; j++) {
      if (i != j) {
        mpz_neg(blabla, shares[j].x);
        mpz_mul(term, term, blabla);
        mpz_mod(term, term, prime);
        mpz_sub(blabla, shares[i].x, shares[j].x);
        mpz_mul(inverse_i, inverse_i, blabla);
        mpz_mod(inverse_i, inverse_i, prime);
      }
    }

    // Si la puissance est toujours 0, inverse_i restera 1
    mpz_sub_ui(blabla, prime, 2);
    mpz_powm(inverse_i, inverse_i, blabla, prime);
    mpz_mul(term, term, inverse_i);
    mpz_mod(term, term, prime);

    mpz_add(result, result, term);
    mpz_add(result, result, prime);
    mpz_mod(result, result, prime);
  }
}

int nombreDejaPresent(int tableau[], int longueur, int nombre) {
  for (int i = 0; i < longueur; i++) {
    if (tableau[i] == nombre) {
      return 1; // Le nombre est déjà présent
    }
  }
  return 0; // Le nombre n'est pas encore présent
}

int *genererTableauAleatoire(int n, int k) {
  if (k > n) {
    printf("Erreur : k ne peut pas être plus grand que n.\n");
    return NULL;
  }

  int *tableau = (int *)malloc(k * sizeof(int));
  if (tableau == NULL) {
    printf("Erreur d'allocation mémoire.\n");
    return NULL;
  }

  int index = 0;

  while (index < k) {
    int nombreAleatoire = rand() % n + 1; // Génère un nombre entre 1 et n

    // Vérifie si le nombre est déjà présent dans le tableau
    if (!nombreDejaPresent(tableau, index, nombreAleatoire)) {
      tableau[index] = nombreAleatoire;
      index++;
    }
  }

  return tableau;
}

int *genererTableauAleatoireWithout(int n, int k, int i) {
  int *tableau = (int *)malloc(k * sizeof(int));
  if (tableau == NULL) {
    printf("Erreur d'allocation mémoire.\n");
    return NULL;
  }

  int index = 0;

  while (index < k) {
    int nombreAleatoire = rand() % n + 1; // Génère un nombre entre 1 et n

    // Vérifie si le nombre est déjà présent dans le tableau
    if (!nombreDejaPresent(tableau, index, nombreAleatoire) &&
        !(nombreAleatoire == i)) {
      tableau[index] = nombreAleatoire;
      index++;
    }
  }

  return tableau;
}

void generate_verification(mpz_t* matrix_verif, mpz_t* matrix, mpz_t g, mpz_t p, int k, int index) {
  mpz_t tmp;
  mpz_init(tmp);

  for (int i = 0; i < k; i++) {
    mpz_powm(tmp, g, matrix[index * k + i], p);
    mpz_init_set(matrix_verif[index * k + i], tmp);
  }
}

void eval_matrix(mpz_t result, mpz_t* matrix, mpz_t q, int k, int index, int point) {
  mpz_t evaluation;
  mpz_inits(result, evaluation, NULL);  

  for (int i = 0; i < k; i++) {
    mpz_set_ui(evaluation, point);
    mpz_powm_ui(evaluation, evaluation, i, q);
    mpz_mul(evaluation, matrix[index * k + i], evaluation);
    mpz_add(result, result, evaluation);
    mpz_mod(result, result, q);
  }
}

void regroupeAllShares(mpz_t* shares, mpz_t* matrix, mpz_t q, int k, int n, int index) {
  int cpt = 0;

  for (int i = 0; i < n; i++) {
    if (i != index) {
      eval_matrix(shares[index * (n-1) + cpt], matrix, q, k, i, index+1);
      cpt++;
    }
  }
}
