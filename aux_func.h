#include "gmp.h"
#define KRED "\x1B[31m"
#define KNRM "\x1B[0m"

// Structure pour représenter un point (x, y)
typedef struct {
  mpz_t x;
  mpz_t y;
} Point;

/* Initializes and generates a polynomial of degrees k-1 with coefficient in q */
void generate_poly(mpz_t* coefficients, mpz_t q, int k, gmp_randstate_t state);

void generate_matrix_poly(mpz_t* matrix, mpz_t q, int k, int i, gmp_randstate_t state);

/* Generate an element g of order q mod p */
void generate_elem_order_q (mpz_t g, mpz_t p, mpz_t q, gmp_randstate_t ssstate);

/* Print the polynom, or the poly at the index in the matrix */
void printPolynomial(mpz_t *coefficients, int degree);
void print_matrix (mpz_t* matrix, int k, int index);

void evaluatePolynome(mpz_t result, mpz_t *coefficient, mpz_t prime, int point,
                      int degree);

/* Generate the Shamir's shares */
Point *generateShares(mpz_t *coefficients, int n, int k, mpz_t prime);

void lagrangeInterpolation(mpz_t result, Point *shares, int k, mpz_t prime);

int nombreDejaPresent(int tableau[], int longueur, int nombre);

int *genererTableauAleatoire(int n, int k) ;

int *genererTableauAleatoireWithout(int n, int k, int i);

void generate_verification(mpz_t* matrix_verif, mpz_t* matrix, mpz_t g, mpz_t p, int k, int index);

void eval_matrix(mpz_t result, mpz_t* matrix, mpz_t q, int k, int index, int point);

void regroupeAllShares(mpz_t shares[], mpz_t *matrix, mpz_t q, int k, int n, int index) ;