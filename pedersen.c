#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "aux_func.h"

int pedersen(mpz_t p, mpz_t q, bool random, bool error){
    printf("==============================================================\n");
    printf("                 Pedersen Protocole Simulation \n"); 
    printf("==============================================================\n\n");

    int n, k;
    int nb_error = -1;
    int *tab_error;
    bool alreadyHere;
    gmp_randstate_t state;
    gmp_randinit_mt(state);
    gmp_randseed_ui(state, (unsigned long int)time(NULL));

    /*=====================================================*/
    /*                                                     */
    /*                     USER'S INPUT                    */
    /*                                                     */
    /*=====================================================*/

    printf("Entrez le nombre total de personnes : ");
    scanf("%d", &n);

    printf("Entrez le seuil de partage : ");
    scanf("%d", &k);

    if (error) {
        while (nb_error < 0 || nb_error > n) {
        printf("Entrez un nombre d'erreur entre %d et %d : ", 0, n);
        scanf("%d", &nb_error);
        }
        tab_error = (int *)malloc(nb_error * sizeof(int));
    }
    if (k > n) {
        printf("Le seuil de partage ne peut pas être supérieur au nombre total"
                "de personnes.\n");
        return 1;
    }

    /*=====================================================*/
    /*                                                     */
    /*                     COMPUTATION                     */
    /*                                                     */
    /*=====================================================*/

    // Recherche d'un element d'ordre q
    mpz_t g;
    mpz_init(g);
    generate_elem_order_q(g,p,q,state);

    /* h une puissance de g */
    mpz_t nb_rand, h;
    mpz_inits(nb_rand, h, NULL);
    mpz_urandomm(nb_rand, state, q);
    mpz_powm(h, g, nb_rand, p);

    // Génération aléatoire des coefficients du polynôme
    mpz_t coefficients[n];
    mpz_t coefficients_2[n];
    for (int i = 0; i < k; i++) {
        mpz_init(coefficients[i]);
        mpz_urandomm(nb_rand, state, q);
        mpz_set(coefficients[i], nb_rand);

        mpz_init(coefficients_2[i]);
        mpz_urandomm(nb_rand, state, q);
        mpz_set(coefficients_2[i], nb_rand);
    }

    // Génération du secret
    mpz_t secret;
    mpz_init_set(secret, coefficients[0]);

    // Génération des partages de Shamir
    Point *shares_1 = generateShares(coefficients, n, k, q);
    Point *shares_2 = generateShares(coefficients_2, n, k, q);

    // Gereration de l'erreur
    if (error) {
        if (random) {
        tab_error = genererTableauAleatoire(n, nb_error);
        for (int i = 0; i < nb_error; i++) {
            tab_error[i]--;
        }

        } else {
        for (int i = 0; i < nb_error; i++) {
            printf("Quel doit etre mauvaise part (choisissez entre 1 et "
                "%d): \n", n);
            scanf("%d", &tab_error[i]);
            tab_error[i]--;

            if (nombreDejaPresent(tab_error, i, tab_error[i]))
                i--;
            }
        }

        for (int i = 0; i < nb_error; i++) {
            mpz_urandomm(nb_rand, state, p);
            mpz_set(shares_1[tab_error[i]].y, nb_rand);

            mpz_urandomm(nb_rand, state, p);
            mpz_set(shares_2[tab_error[i]].y, nb_rand);
        }
    }

    mpz_t verification[k], buf, tmp;
    mpz_inits(buf, tmp, NULL);
    for (int i = 0; i < k; i++) {
        mpz_init(verification[i]);
        mpz_powm(tmp, g, coefficients[i], p);
        mpz_powm(buf, h, coefficients_2[i], p);

        mpz_mul(buf, tmp, buf);
        mpz_mod(buf, buf, p);
        mpz_set(verification[i], buf);
    }

    // choix d'un certain nombre de part aleatoirement:
    int *tableau = NULL;
    Point shares_3[k];

    if (random) {
        tableau = genererTableauAleatoire(n, k);

    } else {
        tableau = (int *)malloc(k * sizeof(int));
        for (int i = 0; i < k; i++) {
        if (error) {
            for (int i = 0; i < nb_error; i++) {
                printf("\n%sLa mauvaise part est la %d\n", KRED, tab_error[i] + 1);
            }
        }
        printf("%schoix de la part %d: ", KNRM, i + 1);
        scanf("%d", &tableau[i]);

        // Gestion d'erreur
        alreadyHere = false;
        for (int j = 0; j < i; j++) {
            if (tableau[i] == tableau[j]) {
                alreadyHere = true;
                break;
            }
        }
        if (tableau[i] > n || tableau[i] < 1 || alreadyHere) {
            printf("vous devez choisir un nombre autre que les précédents entre"
                "%d et %d\n", 1, n);
            i--;
        }
        }
    }
    printf("\n");

    for (int i = 0; i < k; i++) {
        shares_3[i] = shares_1[tableau[i] - 1];
    }

    // Interpolation du polynôme à partir du seuil k
    mpz_t interpolatedSecret;
    mpz_init(interpolatedSecret);
    lagrangeInterpolation(interpolatedSecret, shares_3, k, q);

    /*=====================================================*/
    /*                                                     */
    /*                        PRINTF                       */
    /*                                                     */
    /*=====================================================*/

    gmp_printf("\nLe secret est %Zd\n\n", secret);

    gmp_printf("Le générateur vaut %Zd\n\n", g);
    gmp_printf("h est une puissance de g et vaut %Zd\n", h);

    // Affichage du polynôme généré
    printPolynomial(coefficients, k - 1);
    printPolynomial(coefficients_2, k - 1);

    printf("\n\n=============================\n");
    for (int i = 1; i <= n; i++) {
        if (!nombreDejaPresent(tab_error, nb_error, i - 1)) {
        gmp_printf("%sPartage %d : (%Zd, %Zd, %Zd)\n", KNRM, i, shares_1[i - 1].x,
                shares_1[i - 1].y, shares_2[i - 1].y);
        } else {
        gmp_printf("%sMauvais partage %d : (%Zd, %Zd, %Zd)\n", KRED, i,
                shares_1[i - 1].x, shares_1[i - 1].y, shares_2[i - 1].y);
        }
    }
    printf("%s=============================\n\n", KNRM);

    printf("Le dealer broadcast aussi a chacun:\n");
    for (int i = 0; i < k; i++) {
        gmp_printf("%Zd, ", verification[i]);
    }

    printf("\n\n                VERIFICATION                \n\n");
    mpz_t left, right, sum, pow;
    mpz_inits(left, right, sum, pow, NULL);
    
    for (int i = 0; i < n; i++) {
        mpz_set_ui(sum, 1);
        mpz_set_ui(pow, 1);

        for (int j = 0; j < k; j++) {
            mpz_powm(buf, verification[j], pow, p);
            mpz_mul(sum, sum, buf);
            mpz_mod(sum, sum, p);
            
            mpz_mul_ui(pow, pow, i + 1);
            mpz_mod(pow, pow, q);
        }

        mpz_powm(left, g, shares_1[i].y, p);
        mpz_powm(right, h, shares_2[i].y, p);
        mpz_mul(left, left, right);
        mpz_mod(left, left, p);

        if (mpz_cmp(left, sum)){
            gmp_printf("%sMenteur %Zd = %Zd\n\n", KRED, left, sum);
        }
        else{
            gmp_printf("%s%Zd = %Zd\n\n", KNRM, left, sum);
        }
    }

    for (int i = 0; i < k; i++) {
        if (!(nombreDejaPresent(tab_error, nb_error, tableau[i] - 1))) {
        gmp_printf("%s\nOn utilisera la part %d: (%Zd,%Zd,%Zd)\n", KNRM, tableau[i],
                shares_1[tableau[i] - 1].x, shares_1[tableau[i] - 1].y,
                shares_2[tableau[i] - 1].y);
        } else {
        gmp_printf("%s\nOn utilisera la part %d: (%Zd,%Zd,%Zd)\n", KRED, tableau[i],
                shares_1[tableau[i] - 1].x, shares_1[tableau[i] - 1].y,
                shares_2[tableau[i] - 1].y);
        }
    }
    gmp_printf("\n%sSecret interpolé : %Zd\n", KNRM, interpolatedSecret);
    
    return 0;
}