#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "aux_func.h"

int jf_dkg (mpz_t p, mpz_t q, bool random, bool error, bool ERROR) {
  printf("==============================================================\n");
  printf("                   JF-DKG Protocole Simulation \n"); 
  printf("==============================================================\n\n");
  int n, k, cpt;
  int wrong_index = -1;
  int wrong_index_2 = -1;
  bool alreadyHere;
  gmp_randstate_t state;
  gmp_randinit_mt(state);
  gmp_randseed_ui(state, (unsigned long int)time(NULL));

  /*=====================================================*/
  /*                                                     */
  /*                     USER'S INPUT                    */
  /*                                                     */
  /*=====================================================*/

  printf("Entrez le nombre total de personnes : ");
  scanf("%d", &n);

  printf("Entrez le seuil de partage : ");
  scanf("%d", &k);

  if (k > n) {
    printf("Le seuil de partage ne peut pas être supérieur au nombre total "
            "de personnes.\n");
    return 1;
  }

  /*=====================================================*/
  /*                                                     */
  /*                     COMPUTATION                     */
  /*                                                     */
  /*=====================================================*/

  /* Generate element of order q */
  mpz_t g;
  mpz_init(g);
  generate_elem_order_q(g, p, q, state);

  mpz_t *matrix = malloc( n * k * sizeof(mpz_t) );
  mpz_t *matrix_verif = malloc( n * k * sizeof(mpz_t) );
  mpz_t *shares = malloc( n * (n-1) * sizeof(mpz_t) );
  
  mpz_t zq[n];
  mpz_t yq[n];
  
  for (int i = 0; i < n; i++) {
    generate_matrix_poly(matrix, q, k, i, state);
    mpz_init_set(zq[i], matrix[i * k]);

    generate_verification(matrix_verif, matrix, g, p, k, i);
    mpz_init_set(yq[i], matrix_verif[i * k]);
  }

  for (int i = 0; i < n; i++) {
    regroupeAllShares(shares, matrix, q, k, n, i);
  }


  /* error handling*/
  mpz_t nb_rand;
  mpz_init(nb_rand);
  if (error) {
    if (random) {
      wrong_index = rand() % n;
    } else {
      wrong_index = -1;
      while (wrong_index < 0 || wrong_index > (n - 1)) {
        printf("Quel doit etre la mauvaise part (choisissez entre 1 et %d): \n", n);
        scanf("%d", &wrong_index);
        wrong_index--;
      }
    }

    for (int i = 0; i < n; i++) {
      if (i != wrong_index) {
        mpz_urandomm(nb_rand, state, q);

        if (wrong_index > i) {
          mpz_set(shares[i * (n-1) + (wrong_index - 1)], nb_rand);
        } else {
          mpz_set(shares[i * (n-1) + wrong_index], nb_rand);
        }
      }
    }
  }

  mpz_t y;
  mpz_init_set_ui(y, 1);
  for (int i = 0; i < n; i++) {
    mpz_mul(y, y, yq[i]);
    mpz_mod(y, y, p);
  }

  mpz_t verification[k], buf, tmp;
  mpz_inits(buf, tmp, NULL);
  for (int i = 0; i < k; i++) {
    mpz_init_set_ui(verification[i], 1);

    for (int j = 0; j < n; j++) {
      if (j != wrong_index) {
        mpz_mul(tmp, verification[i], matrix_verif[j * k + i]);
        mpz_mod(verification[i], tmp, p);
      }
    }
  }

  mpz_t x[n];
  for (int i = 0; i < n; i++) {
    mpz_init(x[i]);
    if (i != wrong_index) {
      mpz_set_ui(x[i], 0);

      for (int j = 0; j < (n - 1); j++) {
        if ((wrong_index > i && j != (wrong_index - 1)) ||
            (wrong_index < i && j != wrong_index)) {
              mpz_add(x[i], x[i], shares[i * (n-1) + j]);
              mpz_mod(x[i], x[i], q);
        }
      }

      eval_matrix(tmp, matrix, q, k, i, i+1);
      mpz_add(x[i], x[i], tmp);
      mpz_mod(x[i], x[i], q);
    }
  }

  /* Set secret */
  mpz_t secret;
  mpz_init_set_ui(secret, 0);
  for (int i = 0; i < n; i++) {
    if (i != wrong_index) {
      mpz_add(secret, secret, zq[i]);
      mpz_mod(secret, secret, q);
    }
  }

  int *tableau = NULL;
  Point shares_2[k];

  if (random) {
    tableau = genererTableauAleatoireWithout(n, k, wrong_index + 1);
    wrong_index_2 = rand() % n;

  } else {
    tableau = (int *)malloc(k * sizeof(int));
    for (int i = 0; i < k; i++) {
      printf("%sChoix de la part %d: ", KNRM, i + 1);
      scanf("%d", &tableau[i]);

      // Gestion d'erreur
      alreadyHere = false;
      for (int j = 0; j < i; j++) {
        if (tableau[i] == tableau[j]) {
          alreadyHere = true;
          break;
        }
      }
      if (tableau[i] > n || tableau[i] < 1 || alreadyHere) {
        printf("vous devez choisir un nombre autre que les précédents entre "
                "%d et %d\n",
                1, n);
        i--;
      }
    }
  }

  for (int i = 0; i < k; i++) {
    mpz_init_set_ui(shares_2[i].x, tableau[i]);
    mpz_init_set(shares_2[i].y, x[tableau[i] - 1]);
  }

  if (ERROR) {
    while (!nombreDejaPresent(tableau, k, wrong_index_2 + 1)) {
      wrong_index_2 = rand() % n;
    }
    mpz_urandomm(nb_rand, state, q);
    mpz_set(shares_2[wrong_index_2].y, nb_rand);

    mpz_urandomm(nb_rand, state, q);
    mpz_set(x[wrong_index_2], nb_rand);
  }

  // Interpolation du polynôme à partir du seuil k
  mpz_t interpolatedSecret;
  mpz_init(interpolatedSecret);
  lagrangeInterpolation(interpolatedSecret, shares_2, k, q);

  /*=====================================================*/
  /*                                                     */
  /*                        PRINTF                       */
  /*                                                     */
  /*=====================================================*/

  gmp_printf("Le generateur vaut %Zd\n", g);

  printf("\n=============================== Sharing information "
          "===============================\n");
  if (error) {
    printf("ATTENTION %d à envoyé n'importe quoi\n", (wrong_index + 1));
  }

  mpz_t left, right, sum, pow;
  mpz_inits(left, right, sum, pow, NULL);

  for (int i = 0; i < n; i++) {
    print_matrix(matrix, k, i);
    gmp_printf("\nLe secret correspondant est %Zd\n", zq[i]);
    gmp_printf("\nLa clé publique correspondante est %Zd\n", yq[i]);
    printf("\nCette partie va broadcast: \n");
    for (int j = 0; j < k; j++) {
      gmp_printf("%Zd, ", matrix_verif[i * k + j]);
    }

    printf("\n\nCette partie va recevoir sa part: (");
    for (int j = 0; j < n - 2; j++) {
      gmp_printf(" %Zd,", shares[i * (n-1) + j]);
    }
    gmp_printf(" %Zd)\n", shares[i * (n-1) + (n - 2)]);

    printf("\nIl verifie donc que : \n");
    cpt = 0;
    for (int j = 0; j < n; j++) {
      if (j != i) {
        mpz_set_ui(sum, 1);
        mpz_set_ui(pow, 1);

        for (int l = 0; l < k; l++) {
          mpz_powm(buf, matrix_verif[j * k + l], pow, p);
          mpz_mul(sum, sum, buf);
          mpz_mod(sum, sum, p);
          
          mpz_mul_ui(pow, pow, i + 1);
          mpz_mod(pow, pow, q);
        }

        mpz_powm(left, g, shares[i * (n-1) + cpt], p);
        
        if (mpz_cmp(left, sum)){
          gmp_printf("%sMenteur %Zd = %Zd\n\n", KRED, left, sum);
        }
        else{
            gmp_printf("%s%Zd = %Zd\n\n", KNRM, left, sum);
        }

        cpt++;
      }
    }
  }

  if (error) {
    printf("Les participants se rendant compte que %d faisait n'importe quoi, il est exclus !",
            wrong_index + 1);
  }
  printf("\n\n=============================== Final posessed info "
          "===============================\n\n");
  gmp_printf("Le secret vaut %Zd\n\n===============================================\n", secret);

  if (ERROR) {
    printf("Un autre vilain modifie la valeure n°: %d\n", wrong_index_2 + 1);
  }
  for (int i = 0; i < n; i++) {
    if (i != wrong_index) {
      gmp_printf("Partage : (%d, %Zd)\n", i + 1, x[i]);
    }
  }

  printf("\n\n                VERIFICATION                \n\n");

  for (int i = 0; i < n; i++) {
    if (i != wrong_index) {
      mpz_set_ui(sum, 1);
      mpz_set_ui(pow, 1);

      for (int j = 0; j < k; j++) {
        mpz_powm(buf, verification[j], pow, p);
        mpz_mul(sum, sum, buf);
        mpz_mod(sum, sum, p);

        mpz_mul_ui(pow, pow, i + 1);
        mpz_mod(pow, pow, q);
      }

      mpz_powm(left, g, x[i], p);
      if (mpz_cmp(left, sum)){
          gmp_printf("%sMenteur %Zd = %Zd\n\n", KRED, left, sum);
      }
      else{
          gmp_printf("%s%Zd = %Zd\n\n", KNRM, left, sum);
      }
    }
  }

  for (int i = 0; i < k; i++) {
    if (tableau[i] - 1 != wrong_index_2) {
      gmp_printf("%sOn utilisera la mauvaise part %d: (%Zd,%Zd)\n", KNRM, tableau[i],
              shares_2[i].x, shares_2[i].y);
    } else {
      gmp_printf("%sOn utilisera la part %d: (%Zd,%Zd)\n", KRED, tableau[i],
              shares_2[i].x, shares_2[i].y);
    }
  }

  if (mpz_cmp(interpolatedSecret, secret)){
      gmp_printf("\n%sSecret mal interpolé: %Zd\n", KRED, interpolatedSecret);
  }
  else{
      gmp_printf("\n%sSecret correctement interpolé: %Zd\n", KNRM, interpolatedSecret);
  }

  return 0;
}

