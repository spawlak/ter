
#include "gmp.h"
#include <getopt.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define KRED "\x1B[31m"
#define KNRM "\x1B[0m"
#define QSIZE 256
#define PSIZE 2048

// Structure pour représenter un point (x, y)
typedef struct {
  mpz_t x;
  mpz_t y;
} Point;

// Fonction pour afficher un polynôme
void printPolynomial(mpz_t *coefficients, int degree) {
  printf("Polynôme : ");
  for (int i = degree; i >= 0; i--) {
    if (coefficients[i] != 0) {
      if (i < degree) {
        printf(" + ");
      }
      gmp_printf("%Zdx^%d", coefficients[i], i);
    }
  }
  printf("\n");
}

void evaluatePolynome(mpz_t result, mpz_t *coefficient, mpz_t prime, int point,
                      int degree) {
  mpz_t evaluation;
  mpz_init(result);
  mpz_init(evaluation);
  for (int i = 0; i < degree; i++) {
    mpz_set_ui(evaluation, point);
    mpz_powm_ui(evaluation, evaluation, i, prime);
    mpz_mul(evaluation, coefficient[i], evaluation);
    mpz_add(result, result, evaluation);
    mpz_mod(result, result, prime);
  }
}

// Fonction pour calculer les partages de Shamir
Point *generateShares(mpz_t *coefficients, int n, int k, mpz_t prime) {
  Point *shares = (Point *)malloc(n * sizeof(Point));

  for (int i = 1; i <= n; i++) {
    mpz_init_set_ui(shares[i - 1].x, i);
    mpz_init_set_ui(shares[i - 1].y, 0);
    evaluatePolynome(shares[i - 1].y, coefficients, prime, i, k);
  }

  return shares;
}

void lagrangeInterpolation(mpz_t result, Point *shares, int k, mpz_t prime) {

  mpz_t term, blabla, inverse_i;
  mpz_init(blabla);
  mpz_init(term);
  mpz_init(inverse_i);
  for (int i = 0; i < k; i++) {
    mpz_set(term, shares[i].y);
    mpz_set_ui(inverse_i, 1); // Lorsque x est 0, la puissance est toujours 0

    for (int j = 0; j < k; j++) {
      if (i != j) {
        mpz_neg(blabla, shares[j].x);
        mpz_mul(term, term, blabla);
        mpz_mod(term, term, prime);
        mpz_sub(blabla, shares[i].x, shares[j].x);
        mpz_mul(inverse_i, inverse_i, blabla);
        mpz_mod(inverse_i, inverse_i, prime);
      }
    }

    // Si la puissance est toujours 0, inverse_i restera 1
    mpz_sub_ui(blabla, prime, 2);
    mpz_powm(inverse_i, inverse_i, blabla, prime);
    mpz_mul(term, term, inverse_i);
    mpz_mod(term, term, prime);

    mpz_add(result, result, term);
    mpz_add(result, result, prime);
    mpz_mod(result, result, prime);
  }
}

int nombreDejaPresent(int tableau[], int longueur, int nombre) {
  for (int i = 0; i < longueur; i++) {
    if (tableau[i] == nombre) {
      return 1; // Le nombre est déjà présent
    }
  }
  return 0; // Le nombre n'est pas encore présent
}

int *genererTableauAleatoire(int n, int k) {
  if (k > n) {
    printf("Erreur : k ne peut pas être plus grand que n.\n");
    return NULL;
  }

  int *tableau = (int *)malloc(k * sizeof(int));
  if (tableau == NULL) {
    printf("Erreur d'allocation mémoire.\n");
    return NULL;
  }

  int index = 0;

  while (index < k) {
    int nombreAleatoire = rand() % n + 1; // Génère un nombre entre 1 et n

    // Vérifie si le nombre est déjà présent dans le tableau
    if (!nombreDejaPresent(tableau, index, nombreAleatoire)) {
      tableau[index] = nombreAleatoire;
      index++;
    }
  }

  return tableau;
}

int *genererTableauAleatoireWithout(int n, int k, int i) {
  if (k > n) {
    printf("Erreur : k ne peut pas être plus grand que n.\n");
    return NULL;
  }

  int *tableau = (int *)malloc(k * sizeof(int));
  if (tableau == NULL) {
    printf("Erreur d'allocation mémoire.\n");
    return NULL;
  }

  int index = 0;

  while (index < k) {
    int nombreAleatoire = rand() % n + 1; // Génère un nombre entre 1 et n

    // Vérifie si le nombre est déjà présent dans le tableau
    if (!nombreDejaPresent(tableau, index, nombreAleatoire) &&
        !(nombreAleatoire == i)) {
      tableau[index] = nombreAleatoire;
      index++;
    }
  }

  return tableau;
}

// int *generateVerification(int g, int *coefficient, int prime, int t) {
//   int *verification = (int *)malloc(t * sizeof(int));
//   for (int k = 0; k < t; k++) {
//     verification[k] = power(g, coefficient[k], prime);
//   }
//   return verification;
// }

// int *regroupeAllShares(int **coefficientMatrix, int index, int t, int q,
//                        int n) {
//   int *shares = (int *)malloc((n - 1) * sizeof(int));
//   int cpt = 0;
//   for (int i = 0; i < n; i++) {
//     if (i != index) {
//       shares[cpt] = evaluatePolynome(coefficientMatrix[i], q, index + 1, t);
//       cpt++;
//     }
//   }
//   return shares;
// }

int main(int argc, char *argv[]) {
  // NE PAS TOUCHER CA MARCHE
  gmp_randstate_t state;
  mpz_t n, p, q;

  mpz_init(p);
  gmp_randinit_mt(state);
  // NE PAS TOUCHER CA MARCHE
  gmp_randseed_ui(state, (unsigned long int)time(NULL));
  mpz_urandomb(q, state, QSIZE);
  mpz_nextprime(q, q);

  do {
    mpz_set_ui(p, 1);
    mpz_urandomb(n, state, (PSIZE - QSIZE));
    mpz_mul(p, n, q);
    mpz_add_ui(p, p, 1);
  } while (mpz_probab_prime_p(p, 20) == 0);

  gmp_printf("(%Zd,%Zd)\n", q, p);

  srand(time(NULL));

  bool shamir = false;
  bool feldman = false;
  bool pedersen = false;
  bool random = false;
  bool JF_DKG = false;
  bool NEW_DKG = false;
  bool error = false;
  bool ERROR = false;

  int opt;
  int option_index = 0;
  struct option longopts[] = {{"help", no_argument, 0, 'h'},
                              {"version", no_argument, 0, 'v'},
                              {"shamir", no_argument, 0, 's'},
                              {"feldman", no_argument, 0, 'f'},
                              {"pedersen", no_argument, 0, 'p'},
                              {"random", no_argument, 0, 'r'},
                              {"JF_DKG", no_argument, 0, 'd'},
                              {"NEW_DKG", no_argument, 0, 'n'},
                              {"error", no_argument, 0, 'e'},
                              {"ERROR", no_argument, 0, 'E'},
                              // Ajoutez d'autres options au besoin
                              {0, 0, 0, 0}};

  while ((opt = getopt_long(argc, argv, "hvsfrpderEn", longopts,
                            &option_index)) != -1) {
    switch (opt) {
    case 'h':
      printf("Usage: TER [options]\n");
      printf("Options:\n");
      printf("  -h, --help           Afficher l'aide\n");
      printf("  -v, --version        Afficher la version\n");
      printf("  -s, --shamir         Simule un protocole de shamir\n");
      printf("  -f, --feldman        Simule un protocole de feldman\n");
      printf("  -p, --pedersen       Simule un protocole de pedersen\n");
      printf("  -d, --JF_DKG         Simule un protocole de DKG pedersen\n");
      printf("  -n, --NEW_DKG         Simule un protocole de DKG securisé\n");
      printf("  -r, --random         Simule le choix des parts et de l'erreur "
             "aléatoirement\n");
      printf("  -e, --error          Simule le protocole avec une erreur\n");
      printf("  -E, --ERROR          Simule le protocole avec une erreur a la "
             "deuxieme etape de JF_DKG\n");
      return 0;
    case 'v':
      printf("Sharing_secret_protocoles - Version 1.0\n");
      return 0;
    case 'r':
      random = true;
      break;
    case 'e':
      error = true;
      break;
    case 'E':
      ERROR = true;
      break;
    case 's':
      shamir = true;
      break;
    case 'f':
      feldman = true;
      break;
    case 'p':
      pedersen = true;
      break;
    case 'd':
      JF_DKG = true;
      break;
    case 'n':
      NEW_DKG = true;
      break;
    default:
      abort();
    }
  }

  if (shamir) {
    printf("==============================================================\n");
    printf("                   Shamir Protocole Simulation                \n");
    printf(
        "==============================================================\n\n");

    int n, k;
    bool alreadyHere;

    /*=====================================================*/
    /*                                                     */
    /*                     USER'S INPUT                    */
    /*                                                     */
    /*=====================================================*/

    printf("Entrez le nombre total de personnes : ");
    scanf("%d", &n);

    printf("Entrez le seuil de partage : ");
    scanf("%d", &k);

    if (k > n || k < 1) {
      printf("Le seuil de partage ne peut pas être supérieur au nombre total "
             "de personnes.\n");
      return 1;
    }

    /*=====================================================*/
    /*                                                     */
    /*                     COMPUTATION                     */
    /*                                                     */
    /*=====================================================*/

    // Génération aléatoire des coefficients du polynôme
    mpz_t coefficients[n];
    mpz_t nb_rand;
    mpz_init(nb_rand);
    for (int i = 0; i < k; i++) {
      mpz_init(coefficients[i]);
      mpz_urandomm(nb_rand, state, p);
      mpz_set(coefficients[i], nb_rand);
    }

    mpz_t secret;
    mpz_init_set(secret, coefficients[0]);

    // Génération des partages de Shamir
    Point *shares = generateShares(coefficients, n, k, p);

    // choix d'un certain nombre de part:
    Point shares_2[k];
    int *tableau = NULL;

    if (random) {
      tableau = genererTableauAleatoire(n, k);
    } else {
      tableau = (int *)malloc(k * sizeof(int));
      for (int i = 0; i < k; i++) {
        printf("choix de la part %d:", i + 1);
        scanf("%d", &tableau[i]);

        // Gestion d'erreur
        alreadyHere = false;
        for (int j = 0; j < i; j++) {
          if (tableau[i] == tableau[j]) {
            alreadyHere = true;
            break;
          }
        }
        if (tableau[i] > n || tableau[i] < 1 || alreadyHere) {
          printf("vous devez choisir un nombre autre que les précédents entre "
                 "%d et %d\n",
                 1, n);
          i--;
        }
      }
    }
    for (int i = 0; i < k; i++) {
      shares_2[i] = shares[tableau[i] - 1];
    }

    // Interpolation du polynôme à partir du seuil k
    mpz_t interpolatedSecret;
    mpz_init(interpolatedSecret);
    lagrangeInterpolation(interpolatedSecret, shares_2, k, p);

    /*=====================================================*/
    /*                                                     */
    /*                        PRINTF                       */
    /*                                                     */
    /*=====================================================*/

    gmp_printf("le secret vaut %Zd", secret);
    printPolynomial(coefficients, k - 1);
    // print des parts:
    printf("\n\n=============================\n");
    for (int i = 1; i <= n; i++) {
      gmp_printf("Partage %d : (%Zd, %Zd)\n", i, shares[i - 1].x,
                 shares[i - 1].y);
    }

    printf("=============================\n\n");
    for (int i = 0; i < k; i++) {
      gmp_printf("on utilisera la part %d: (%Zd,%Zd)\n", tableau[i],
                 shares_2[i].x, shares_2[i].y);
    }
    gmp_printf("le secret interpolé est %Zd", interpolatedSecret);
  }

  if (feldman) {
    printf("==============================================================\n");
    printf("                  Feldman Protocole Simulation \n");
    printf(
        "==============================================================\n\n");

    int n, k, prime;
    int nb_error = -1;
    int *tab_error;
    bool alreadyHere;

    /*=====================================================*/
    /*                                                     */
    /*                     USER'S INPUT                    */
    /*                                                     */
    /*=====================================================*/

    printf("Entrez le nombre total de personnes : ");
    scanf("%d", &n);

    printf("Entrez le seuil de partage : ");
    scanf("%d", &k);

    if (error) {
      while (nb_error < 0 || nb_error > n) {
        printf("Entrez un nombre d'erreur entre %d et %d : ", 0, n);
        scanf("%d", &nb_error);
      }
      tab_error = (int *)malloc(nb_error * sizeof(int));
    }
    if (k > n) {
      printf("Le seuil de partage ne peut pas être supérieur au nombre total"
             "de personnes.\n");
      return 1;
    }

    /*=====================================================*/
    /*                                                     */
    /*                     COMPUTATION                     */
    /*                                                     */
    /*=====================================================*/

    // Génération aléatoire des coefficients du polynôme

    mpz_t coefficients[n];
    mpz_t nb_rand;
    mpz_init(nb_rand);
    for (int i = 0; i < k; i++) {
      mpz_init(coefficients[i]);
      mpz_urandomm(nb_rand, state, p);
      mpz_set(coefficients[i], nb_rand);
    }

    mpz_t secret;
    mpz_init_set(secret, coefficients[0]);

    // Recherche d'un element d'ordre q
    mpz_t g, temps;
    mpz_inits(g, temps, NULL);
    while (mpz_cmp_ui(g, 1) && !mpz_cmp_ui(g, 0)) {
      mpz_urandomm(g, state, p);
      mpz_sub_ui(temps, p, 1);
      mpz_div(temps, temps, q);
      mpz_powm(g, g, temps, p);
    }

    // Génération des partages de Shamir
    Point *shares = generateShares(coefficients, n, k, p);

    // Gereration de l'erreur
    if (error) {
      if (random) {

        tab_error = genererTableauAleatoire(n, nb_error);
        for (int i = 0; i < nb_error; i++) {
          tab_error[i]--;
        }

      } else {
        for (int i = 0; i < nb_error; i++) {
          printf("Quel doit etre mauvaise part (choisissez entre 1 et "
                 "%d): \n",
                 n);
          scanf("%d", &tab_error[i]);
          tab_error[i]--;
          if (nombreDejaPresent(tab_error, i, tab_error[i]))
            i--;
        }
      }
      mpz_t nb_rand;
      mpz_init(nb_rand);
      for (int i = 0; i < nb_error; i++) {
        mpz_urandomm(nb_rand, state, p);
        mpz_set(shares[tab_error[i]].y, nb_rand);
      }
    }

    mpz_t verification[k];
    for (int i = 0; i < k; i++) {
      mpz_init(verification[i]);
      mpz_powm(verification[i], g, coefficients[i], p);
    }
    printf("ici\n");

    // choix d'un certain nombre de part aleatoirement:
    Point shares_2[k];
    int *tableau = NULL;

    if (random) {
      tableau = genererTableauAleatoire(n, k);

    } else {
      tableau = (int *)malloc(k * sizeof(int));
      for (int i = 0; i < k; i++) {
        if (error) {
          for (int i = 0; i < nb_error; i++) {
            printf("\n%sla mauvaise part est la %d\n", KRED, tab_error[i] + 1);
          }
        }
        printf("%schoix de la part %d: ", KNRM, i + 1);
        scanf("%d", &tableau[i]);

        // Gestion d'erreur
        alreadyHere = false;
        for (int j = 0; j < i; j++) {
          if (tableau[i] == tableau[j]) {
            alreadyHere = true;
            break;
          }
        }
        if (tableau[i] > n || tableau[i] < 1 || alreadyHere) {
          printf("vous devez choisir un nombre autre que les précédents entre "
                 "%d et %d\n",
                 1, n);
          i--;
        }
      }
    }
    for (int i = 0; i < k; i++) {
      shares_2[i] = shares[tableau[i] - 1];
    }

    // Interpolation du polynôme à partir du seuil k
    mpz_t interpolatedSecret;
    mpz_init(interpolatedSecret);
    lagrangeInterpolation(interpolatedSecret, shares_2, k, p);

    /*=====================================================*/
    /*                                                     */
    /*                        PRINTF                       */
    /*                                                     */
    /*=====================================================*/

    gmp_printf("\nle secret vaut %Zd\n", secret);

    // Affichage du polynôme généré
    printPolynomial(coefficients, k - 1);

    gmp_printf("\n\nle générateur vaut %Zd\n", g);

    printf("\n\n=============================\n");
    for (int i = 1; i <= n; i++) {
      if (!nombreDejaPresent(tab_error, nb_error, i - 1)) {
        gmp_printf("%sPartage %d : (%Zd, %Zd)\n", KNRM, i, shares[i - 1].x,
                   shares[i - 1].y);
      } else {
        gmp_printf("%sMauvais partage %d : (%Zd, %Zd)\n", KRED, i,
                   shares[i - 1].x, shares[i - 1].y);
      }
    }
    printf("%s=============================\n\n", KNRM);

    printf("Le dealer broadcast aussi a chacun:\n");

    for (int i = 0; i < k; i++) {
      gmp_printf("%Zd, ", verification[i]);
    }

    printf("\n\n                VERIFICATION                \n\n");
    mpz_t left, sum, pow, temp;
    mpz_inits(left, sum, pow, temp, NULL);
    for (int i = 0; i < n; i++) {
      mpz_set_ui(sum, 1);
      mpz_set_ui(pow, 1);
      for (int j = 0; j < k; j++) {
        mpz_powm(temp, verification[j], pow, p);
        mpz_mul(sum, sum, temp);
        mpz_mod(sum, sum, p);
        mpz_mul_ui(pow, pow, i + 1);
        mpz_mod(pow, pow, q);
      }
      mpz_mod(sum, sum, p);
      mpz_powm(left, g, shares[i].y, p);
      gmp_printf("%Zd = %Zd\n", left, sum);
    }

    printf("\n");
    for (int i = 0; i < k; i++) {
      if (!(nombreDejaPresent(tab_error, nb_error, tableau[i] - 1))) {
        gmp_printf("%son utilisera la part %d: (%Zd,%Zd)\n", KNRM, tableau[i],
                   shares_2[i].x, shares_2[i].y);
      } else {
        gmp_printf("%son utilisera la part %d: (%Zd,%Zd)\n", KRED, tableau[i],
                   shares_2[i].x, shares_2[i].y);
      }
    }

    gmp_printf("\n%sSecret interpolé : %Zd\n", KNRM, interpolatedSecret);

    // Libération de la mémoire
  }

  // if (pedersen) {
  //   printf("==============================================================\n");
  //   printf("                 Pedersen Protocole Simulation \n"); printf(
  //       "==============================================================\n\n");

  //   int n, k, prime, q, g, h;
  //   int nb_error = -1;
  //   int *tab_error;
  //   bool alreadyHere;

  //   /*=====================================================*/
  //   /*                                                     */
  //   /*                     USER'S INPUT                    */
  //   /*                                                     */
  //   /*=====================================================*/

  //   printf("Entrez le nombre total de personnes : ");
  //   scanf("%d", &n);

  //   printf("Entrez le seuil de partage : ");
  //   scanf("%d", &k);

  //   printf("Entrez un nombre premier : ");
  //   scanf("%d", &prime);
  //   printf("Entrez son binome : ");
  //   scanf("%d", &q);
  //   if (error) {
  //     while (nb_error < 0 || nb_error > n) {
  //       printf("Entrez un nombre d'erreur entre %d et %d : ", 0, n);
  //       scanf("%d", &nb_error);
  //     }
  //     tab_error = (int *)malloc(nb_error * sizeof(int));
  //   }
  //   if (k > n) {
  //     printf("Le seuil de partage ne peut pas être supérieur au nombre
  //     total
  //     "
  //            "de personnes.\n");
  //     return 1;
  //   }

  //   /*=====================================================*/
  //   /*                                                     */
  //   /*                     COMPUTATION                     */
  //   /*                                                     */
  //   /*=====================================================*/

  //   // Recherche d'un element d'ordre q
  //   g = generateElementOfOrder(q, prime);
  //   h = power(g, rand() % q, prime);

  //   // Génération aléatoire des coefficients du polynôme
  //   int *coefficients = generateCoefficient(k, q);
  //   int *coefficients_2 = generateCoefficient(k, q);

  //   // Génération aléatoire du secret
  //   int secret = coefficients[0];

  //   // Génération des partages de Shamir
  //   Point *shares_1 = generateShares(coefficients, n, k, q);
  //   Point *shares_2 = generateShares(coefficients_2, n, k, q);

  //   // Gereration de l'erreur

  //   if (error) {
  //     if (random) {

  //       tab_error = genererTableauAleatoire(n, nb_error);
  //       for (int i = 0; i < nb_error; i++) {
  //         tab_error[i]--;
  //       }

  //     } else {
  //       for (int i = 0; i < nb_error; i++) {
  //         printf("Quel doit etre mauvaise part (choisissez entre 1 et "
  //                "%d): \n",
  //                n);
  //         scanf("%d", &tab_error[i]);
  //         tab_error[i]--;
  //         if (nombreDejaPresent(tab_error, i, tab_error[i]))
  //           i--;
  //       }
  //     }
  //     for (int i = 0; i < nb_error; i++) {
  //       shares_1[tab_error[i]].y = rand() % q;
  //       shares_2[tab_error[i]].y = rand() % q;
  //     }
  //   }

  //   int verification[k];
  //   for (int i = 0; i < k; i++) {
  //     verification[i] = (power(g, coefficients[i], prime) *
  //                        power(h, coefficients_2[i], prime)) %
  //                       prime;
  //   }

  //   // choix d'un certain nombre de part aleatoirement:
  //   int *tableau = NULL;
  //   Point shares_3[k];

  //   if (random) {
  //     tableau = genererTableauAleatoire(n, k);

  //   } else {
  //     tableau = (int *)malloc(k * sizeof(int));
  //     for (int i = 0; i < k; i++) {
  //       if (error) {
  //         for (int i = 0; i < nb_error; i++) {
  //           printf("\n%sla mauvaise part est la %d\n", KRED, tab_error[i] +
  //           1);
  //         }
  //       }
  //       printf("%schoix de la part %d: ", KNRM, i + 1);
  //       scanf("%d", &tableau[i]);

  //       // Gestion d'erreur
  //       alreadyHere = false;
  //       for (int j = 0; j < i; j++) {
  //         if (tableau[i] == tableau[j]) {
  //           alreadyHere = true;
  //           break;
  //         }
  //       }
  //       if (tableau[i] > n || tableau[i] < 1 || alreadyHere) {
  //         printf("vous devez choisir un nombre autre que les précédents
  //         entre
  //         "
  //                "%d et %d\n",
  //                1, n);
  //         i--;
  //       }
  //     }
  //   }
  //   printf("\n");

  //   for (int i = 0; i < k; i++) {
  //     shares_3[i] = shares_1[tableau[i] - 1];
  //   }

  //   // Interpolation du polynôme à partir du seuil k
  //   int interpolatedSecret = lagrangeInterpolation(shares_3, k, q);

  //   /*=====================================================*/
  //   /*                                                     */
  //   /*                        PRINTF                       */
  //   /*                                                     */
  //   /*=====================================================*/

  //   printf("\n The secret is %d\n", secret);

  //   printf("Le générateur vaut %d\n", g);
  //   printf("h est une puissance de g et vaut %d\n", h);

  //   // Affichage du polynôme généré
  //   printPolynomial(coefficients, k - 1, prime);
  //   printPolynomial(coefficients_2, k - 1, prime);

  //   printf("\n\n=============================\n");

  //   for (int i = 1; i <= n; i++) {
  //     if (!nombreDejaPresent(tab_error, nb_error, i - 1)) {
  //       printf("%sPartage %d : (%d, %d, %d)\n", KNRM, i, shares_1[i - 1].x,
  //              shares_1[i - 1].y, shares_2[i - 1].y);
  //     } else {
  //       printf("%sMauvais partage %d : (%d, %d, %d)\n", KRED, i,
  //              shares_1[i - 1].x, shares_1[i - 1].y, shares_2[i - 1].y);
  //     }
  //   }
  //   printf("%s=============================\n\n", KNRM);

  //   printf("Le dealer broadcast aussi a chacun:\n");
  //   for (int i = 0; i < k; i++) {
  //     printf("%d, ", verification[i]);
  //   }

  //   printf("\n\n                VERIFICATION                \n\n");

  //   for (int i = 0; i < n; i++) {
  //     int sum = 1;
  //     int pow = 1;
  //     for (int j = 0; j < k; j++) {
  //       sum = (sum * power(verification[j], pow, prime)) % prime;
  //       pow = (pow * (i + 1)) % q;
  //     }
  //     printf("%d = %d\n",
  //            (power(g, shares_1[i].y, prime) * power(h, shares_2[i].y,
  //            prime)) %
  //                prime,
  //            sum);
  //   }

  //   for (int i = 0; i < k; i++) {
  //     if (!(nombreDejaPresent(tab_error, nb_error, tableau[i] - 1))) {
  //       printf("%son utilisera la part %d: (%d,%d,%d)\n", KNRM, tableau[i],
  //              shares_1[tableau[i] - 1].x, shares_1[tableau[i] - 1].y,
  //              shares_2[tableau[i] - 1].y);
  //     } else {
  //       printf("%son utilisera la part %d: (%d,%d,%d)\n", KRED, tableau[i],
  //              shares_1[tableau[i] - 1].x, shares_1[tableau[i] - 1].y,
  //              shares_2[tableau[i] - 1].y);
  //     }
  //   }
  //   printf("\n%sSecret interpolé : %d\n", KNRM, interpolatedSecret);

  //   // Libération de la mémoire
  //   free(coefficients);
  //   free(coefficients_2);
  //   free(shares_1);
  //   free(shares_2);
  //   free(tableau);
  // }
  // if (JF_DKG) {
  //   printf("==============================================================\n");
  //   printf("                   JF-DKG Protocole Simulation \n"); printf(
  //       "==============================================================\n\n");
  //   int n, t, q, prime, g, cpt, y, sum, pow;
  //   int wrong_index = -1;
  //   int wrong_index_2 = -1;
  //   bool alreadyHere;

  //   /*=====================================================*/
  //   /*                                                     */
  //   /*                     USER'S INPUT                    */
  //   /*                                                     */
  //   /*=====================================================*/

  //   printf("Entrez le nombre total de personnes : ");
  //   scanf("%d", &n);

  //   printf("Entrez le seuil de partage : ");
  //   scanf("%d", &t);

  //   printf("Entrez un nombre premier : ");
  //   scanf("%d", &prime);
  //   printf("Entrez son binome : ");
  //   scanf("%d", &q);

  //   if (t > n) {
  //     printf("Le seuil de partage ne peut pas être supérieur au nombre
  //     total
  //     "
  //            "de personnes.\n");
  //     return 1;
  //   }

  //   /*=====================================================*/
  //   /*                                                     */
  //   /*                     COMPUTATION                     */
  //   /*                                                     */
  //   /*=====================================================*/

  //   g = generateElementOfOrder(q, prime);

  //   int **coefficientMatrix = (int **)malloc(n * sizeof(int *));
  //   int **verificationMatrix = (int **)malloc(n * sizeof(int *));
  //   int **shares = (int **)malloc(n * sizeof(int *));
  //   int zq[n];
  //   int yq[n];

  //   for (int i = 0; i < n; i++) {
  //     coefficientMatrix[i] = generateCoefficient(t, q);
  //     zq[i] = coefficientMatrix[i][0];
  //     verificationMatrix[i] =
  //         generateVerification(g, coefficientMatrix[i], prime, t);
  //     yq[i] = verificationMatrix[i][0];
  //   }
  //   for (int i = 0; i < n; i++) {
  //     shares[i] = regroupeAllShares(coefficientMatrix, i, t, q, n);
  //   }
  //   if (error) {
  //     if (random) {
  //       wrong_index = rand() % n;
  //     } else {
  //       wrong_index = -1;
  //       while (wrong_index < 0 || wrong_index > (n - 1)) {
  //         printf("Quel doit etre la mauvaise part (choisissez entre 1 et "
  //                "%d): \n",
  //                n);
  //         scanf("%d", &wrong_index);
  //         wrong_index--;
  //       }
  //     }
  //     for (int i = 0; i < n; i++) {
  //       if (i != wrong_index) {
  //         if (wrong_index > i) {
  //           shares[i][wrong_index - 1] = rand() % q;
  //         } else {
  //           shares[i][wrong_index] = rand() % q;
  //         }
  //       }
  //     }
  //   }

  //   y = 1;
  //   for (int i = 0; i < n; i++) {
  //     y = (y * yq[i]) % prime;
  //   }

  //   int *verification = (int *)malloc(t * sizeof(int));
  //   for (int i = 0; i < t; i++) {
  //     verification[i] = 1;
  //     for (int j = 0; j < n; j++) {
  //       if (j != wrong_index) {
  //         verification[i] =
  //             (verification[i] * verificationMatrix[j][i]) % prime;
  //       }
  //     }
  //   }

  //   int *x = (int *)malloc(n * sizeof(int));
  //   for (int i = 0; i < n; i++) {
  //     if (i != wrong_index) {
  //       x[i] = 0;
  //       for (int j = 0; j < (n - 1); j++) {
  //         if ((wrong_index > i && j != (wrong_index - 1)) ||
  //             (wrong_index < i && j != wrong_index)) {
  //           x[i] = (x[i] + shares[i][j]) % q;
  //         }
  //       }
  //       x[i] = (x[i] + evaluatePolynome(coefficientMatrix[i], q, i + 1, t))
  //       % q;
  //     }
  //   }

  //   int secret = 0;
  //   for (int i = 0; i < n; i++) {
  //     if (i != wrong_index) {
  //       secret = (secret + zq[i]) % q;
  //     }
  //   }

  //   int *tableau = NULL;
  //   Point shares_2[t];

  //   if (random) {
  //     tableau = genererTableauAleatoireWithout(n, t, wrong_index + 1);
  //     wrong_index_2 = rand() % n;
  //   } else {
  //     tableau = (int *)malloc(t * sizeof(int));
  //     for (int i = 0; i < t; i++) {
  //       printf("%schoix de la part %d:", KNRM, i + 1);
  //       scanf("%d", &tableau[i]);

  //       // Gestion d'erreur
  //       alreadyHere = false;
  //       for (int j = 0; j < i; j++) {
  //         if (tableau[i] == tableau[j]) {
  //           alreadyHere = true;
  //           break;
  //         }
  //       }
  //       if (tableau[i] > n || tableau[i] < 1 || alreadyHere) {
  //         printf("vous devez choisir un nombre autre que les précédents
  //         entre
  //         "
  //                "%d et %d\n",
  //                1, n);
  //         i--;
  //       }
  //     }
  //   }

  //   for (int i = 0; i < t; i++) {
  //     shares_2[i].x = tableau[i];
  //     shares_2[i].y = x[tableau[i] - 1];
  //   }

  //   if (ERROR) {
  //     while (!nombreDejaPresent(tableau, t, wrong_index_2 + 1)) {
  //       wrong_index_2 = rand() % n;
  //     }
  //     shares_2[wrong_index_2].y = rand() % q;
  //     x[wrong_index_2] = rand() % q;
  //   }

  //   int interpolatedSecret = lagrangeInterpolation(shares_2, t, q);

  //   /*=====================================================*/
  //   /*                                                     */
  //   /*                        PRINTF                       */
  //   /*                                                     */
  //   /*=====================================================*/

  //   printf("Le generateur vaut %d\n", g);

  //   printf("=============================== Sharing information "
  //          "===============================\n\n");
  //   if (error) {
  //     printf("ATTENTION %d à envoyé n'importe quoi\n", wrong_index + 1);
  //   }
  //   for (int i = 0; i < n; i++) {
  //     printf("\n");
  //     printPolynomial(coefficientMatrix[i], t, q);
  //     printf("le secret correspondant est %d\n", zq[i]);
  //     printf("la clé publique correspondante est %d\n", yq[i]);
  //     printf("Cette partie va broadcast: \n");
  //     for (int k = 0; k < t; k++) {
  //       printf("%d, ", verificationMatrix[i][k]);
  //     }
  //     printf("\n");
  //     printf("Cette partie va recevoir sa part: (");
  //     for (int k = 0; k < n - 2; k++) {
  //       printf(" %d,", shares[i][k]);
  //     }
  //     printf(" %d)\n", shares[i][n - 2]);

  //     printf("Il verifie donc que : \n");
  //     cpt = 0;
  //     for (int j = 0; j < n; j++) {
  //       if (j != i) {
  //         int sum = 1;
  //         int pow = 1;
  //         for (int k = 0; k < t; k++) {
  //           sum = (sum * power(verificationMatrix[j][k], pow, prime)) %
  //           prime; pow = (pow * (i + 1)) % q;
  //         }
  //         if (power(g, shares[i][cpt], prime) == sum) {
  //           printf("%s%d = %d\n", KNRM, power(g, shares[i][cpt], prime),
  //           sum);
  //         } else {
  //           printf("%s%d = %d\n", KRED, power(g, shares[i][cpt], prime),
  //           sum);
  //         }
  //         printf("%s", KNRM);
  //         cpt++;
  //       }
  //     }
  //   }

  //   if (error) {
  //     printf("se rendant compte que %d faisait n'importe quoi il à été
  //     exclus",
  //            wrong_index + 1);
  //   }
  //   printf("\n\n=============================== Final posessed info "
  //          "===============================\n\n");
  //   printf("Le secret vaut %d\n\n============================\n", secret);

  //   if (ERROR) {
  //     printf("Un autre vilain modifie la valeure n°: %d\n", wrong_index_2 +
  //     1);
  //   }
  //   for (int i = 0; i < n; i++) {
  //     if (i != wrong_index) {
  //       printf("Partage : (%d, %d)\n", i + 1, x[i]);
  //     }
  //   }

  //   printf("\n\n                VERIFICATION                \n\n");

  //   for (int i = 0; i < n; i++) {
  //     if (i != wrong_index) {
  //       int sum = 1;
  //       int pow = 1;
  //       for (int j = 0; j < t; j++) {
  //         sum = (sum * power(verification[j], pow, prime)) % prime;
  //         pow = (pow * (i + 1)) % q;
  //       }
  //       if (power(g, x[i], prime) == sum) {
  //         printf("%s%d = %d\n", KNRM, power(g, x[i], prime), sum);
  //       } else {
  //         printf("%s%d = %d\n", KRED, power(g, x[i], prime), sum);
  //       }
  //     }
  //   }

  //   for (int i = 0; i < t; i++) {
  //     if (tableau[i] - 1 != wrong_index_2) {
  //       printf("%son utilisera la part %d: (%d,%d)\n", KNRM, tableau[i],
  //              shares_2[i].x, shares_2[i].y);
  //     } else {
  //       printf("%son utilisera la part %d: (%d,%d)\n", KRED, tableau[i],
  //              shares_2[i].x, shares_2[i].y);
  //     }
  //   }

  //   printf("\n%sSecret interpolé : %d\n", KNRM, interpolatedSecret);
  //   printf("\n");
  // }

  // if (NEW_DKG) {
  //   printf("==============================================================\n");
  //   printf("                   NEW-DKG Protocole Simulation \n"); printf(
  //       "==============================================================\n\n");
  //   int n, t, q, prime, g, cpt, y, sum, pow;
  //   int wrong_index = -1;
  //   int wrong_index_2 = -1;
  //   bool alreadyHere;

  //   /*=====================================================*/
  //   /*                                                     */
  //   /*                     USER'S INPUT                    */
  //   /*                                                     */
  //   /*=====================================================*/

  //   printf("Entrez le nombre total de personnes : ");
  //   scanf("%d", &n);

  //   printf("Entrez le seuil de partage : ");
  //   scanf("%d", &t);

  //   printf("Entrez un nombre premier : ");
  //   scanf("%d", &prime);
  //   printf("Entrez son binome : ");
  //   scanf("%d", &q);

  //   if (t > n) {
  //     printf("Le seuil de partage ne peut pas être supérieur au nombre
  //     total
  //     "
  //            "de personnes.\n");
  //     return 1;
  //   }

  //   /*=====================================================*/
  //   /*                                                     */
  //   /*                     COMPUTATION                     */
  //   /*                                                     */
  //   /*=====================================================*/

  //   g = generateElementOfOrder(q, prime);

  //   int **coefficientMatrix = (int **)malloc(n * sizeof(int *));
  //   int **verificationMatrix = (int **)malloc(n * sizeof(int *));
  //   int **shares = (int **)malloc(n * sizeof(int *));
  //   int zq[n];
  //   int yq[n];

  //   for (int i = 0; i < n; i++) {
  //     coefficientMatrix[i] = generateCoefficient(t, q);
  //     zq[i] = coefficientMatrix[i][0];
  //     verificationMatrix[i] =
  //         generateVerification(g, coefficientMatrix[i], prime, t);
  //     yq[i] = verificationMatrix[i][0];
  //   }
  //   for (int i = 0; i < n; i++) {
  //     shares[i] = regroupeAllShares(coefficientMatrix, i, t, q, n);
  //   }
  //   if (error) {
  //     if (random) {
  //       wrong_index = rand() % n;
  //     } else {
  //       wrong_index = -1;
  //       while (wrong_index < 0 || wrong_index > (n - 1)) {
  //         printf("Quel doit etre la mauvaise part (choisissez entre 1 et "
  //                "%d): \n",
  //                n);
  //         scanf("%d", &wrong_index);
  //         wrong_index--;
  //       }
  //     }
  //     for (int i = 0; i < n; i++) {
  //       if (i != wrong_index) {
  //         if (wrong_index > i) {
  //           shares[i][wrong_index - 1] = rand() % q;
  //         } else {
  //           shares[i][wrong_index] = rand() % q;
  //         }
  //       }
  //     }
  //   }

  //   y = 1;
  //   for (int i = 0; i < n; i++) {
  //     y = (y * yq[i]) % prime;
  //   }

  //   int *verification = (int *)malloc(t * sizeof(int));
  //   for (int i = 0; i < t; i++) {
  //     verification[i] = 1;
  //     for (int j = 0; j < n; j++) {
  //       if (j != wrong_index) {
  //         verification[i] =
  //             (verification[i] * verificationMatrix[j][i]) % prime;
  //       }
  //     }
  //   }

  //   int *x = (int *)malloc(n * sizeof(int));
  //   for (int i = 0; i < n; i++) {
  //     if (i != wrong_index) {
  //       x[i] = 0;
  //       for (int j = 0; j < (n - 1); j++) {
  //         if ((wrong_index > i && j != (wrong_index - 1)) ||
  //             (wrong_index < i && j != wrong_index)) {
  //           x[i] = (x[i] + shares[i][j]) % q;
  //         }
  //       }
  //       x[i] = (x[i] + evaluatePolynome(coefficientMatrix[i], q, i + 1, t))
  //       % q;
  //     }
  //   }

  //   int secret = 0;
  //   for (int i = 0; i < n; i++) {
  //     if (i != wrong_index) {
  //       secret = (secret + zq[i]) % q;
  //     }
  //   }

  //   int *tableau = NULL;
  //   Point shares_2[t];

  //   if (random) {
  //     tableau = genererTableauAleatoireWithout(n, t, wrong_index + 1);
  //     wrong_index_2 = rand() % n;
  //   } else {
  //     tableau = (int *)malloc(t * sizeof(int));
  //     for (int i = 0; i < t; i++) {
  //       printf("%schoix de la part %d:", KNRM, i + 1);
  //       scanf("%d", &tableau[i]);

  //       // Gestion d'erreur
  //       alreadyHere = false;
  //       for (int j = 0; j < i; j++) {
  //         if (tableau[i] == tableau[j]) {
  //           alreadyHere = true;
  //           break;
  //         }
  //       }
  //       if (tableau[i] > n || tableau[i] < 1 || alreadyHere) {
  //         printf("vous devez choisir un nombre autre que les précédents
  //         entre
  //         "
  //                "%d et %d\n",
  //                1, n);
  //         i--;
  //       }
  //     }
  //   }

  //   for (int i = 0; i < t; i++) {
  //     shares_2[i].x = tableau[i];
  //     shares_2[i].y = x[tableau[i] - 1];
  //   }

  //   if (ERROR) {
  //     while (!nombreDejaPresent(tableau, t, wrong_index_2 + 1)) {
  //       wrong_index_2 = rand() % n;
  //     }
  //     shares_2[wrong_index_2].y = rand() % q;
  //     x[wrong_index_2] = rand() % q;
  //   }

  //   int interpolatedSecret = lagrangeInterpolation(shares_2, t, q);

  //   /*=====================================================*/
  //   /*                                                     */
  //   /*                        PRINTF                       */
  //   /*                                                     */
  //   /*=====================================================*/

  //   printf("Le generateur vaut %d\n", g);

  //   printf("=============================== Sharing information "
  //          "===============================\n\n");
  //   if (error) {
  //     printf("ATTENTION %d à envoyé n'importe quoi\n", wrong_index + 1);
  //   }
  //   for (int i = 0; i < n; i++) {
  //     printf("\n");
  //     printPolynomial(coefficientMatrix[i], t, q);
  //     printf("le secret correspondant est %d\n", zq[i]);
  //     printf("la clé publique correspondante est %d\n", yq[i]);
  //     printf("Cette partie va broadcast: \n");
  //     for (int k = 0; k < t; k++) {
  //       printf("%d, ", verificationMatrix[i][k]);
  //     }
  //     printf("\n");
  //     printf("Cette partie va recevoir sa part: (");
  //     for (int k = 0; k < n - 2; k++) {
  //       printf(" %d,", shares[i][k]);
  //     }
  //     printf(" %d)\n", shares[i][n - 2]);

  //     printf("Il verifie donc que : \n");
  //     cpt = 0;
  //     for (int j = 0; j < n; j++) {
  //       if (j != i) {
  //         int sum = 1;
  //         int pow = 1;
  //         for (int k = 0; k < t; k++) {
  //           sum = (sum * power(verificationMatrix[j][k], pow, prime)) %
  //           prime; pow = (pow * (i + 1)) % q;
  //         }
  //         if (power(g, shares[i][cpt], prime) == sum) {
  //           printf("%s%d = %d\n", KNRM, power(g, shares[i][cpt], prime),
  //           sum);
  //         } else {
  //           printf("%s%d = %d\n", KRED, power(g, shares[i][cpt], prime),
  //           sum);
  //         }
  //         printf("%s", KNRM);
  //         cpt++;
  //       }
  //     }
  //   }

  //   if (error) {
  //     printf("se rendant compte que %d faisait n'importe quoi il à été
  //     exclus",
  //            wrong_index + 1);
  //   }
  //   printf("\n\n=============================== Final posessed info "
  //          "===============================\n\n");
  //   printf("Le secret vaut %d\n\n============================\n", secret);

  //   if (ERROR) {
  //     printf("Un autre vilain modifie la valeure n°: %d\n", wrong_index_2 +
  //     1);
  //   }
  //   for (int i = 0; i < n; i++) {
  //     if (i != wrong_index) {
  //       printf("Partage : (%d, %d)\n", i + 1, x[i]);
  //     }
  //   }

  //   printf("\n\n                VERIFICATION                \n\n");

  //   for (int i = 0; i < n; i++) {
  //     if (i != wrong_index) {
  //       int sum = 1;
  //       int pow = 1;
  //       for (int j = 0; j < t; j++) {
  //         sum = (sum * power(verification[j], pow, prime)) % prime;
  //         pow = (pow * (i + 1)) % q;
  //       }
  //       if (power(g, x[i], prime) == sum) {
  //         printf("%s%d = %d\n", KNRM, power(g, x[i], prime), sum);
  //       } else {
  //         printf("%s%d = %d\n", KRED, power(g, x[i], prime), sum);
  //       }
  //     }
  //   }

  //   for (int i = 0; i < t; i++) {
  //     if (tableau[i] - 1 != wrong_index_2) {
  //       printf("%son utilisera la part %d: (%d,%d)\n", KNRM, tableau[i],
  //              shares_2[i].x, shares_2[i].y);
  //     } else {
  //       printf("%son utilisera la part %d: (%d,%d)\n", KRED, tableau[i],
  //              shares_2[i].x, shares_2[i].y);
  //     }
  //   }

  //   printf("\n%sSecret interpolé : %d\n", KNRM, interpolatedSecret);
  //   printf("\n");
  // }

  return 0;
}
